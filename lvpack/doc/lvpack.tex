%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%2015/10/05 Keiichi Ishioka (version 0.0)
%2016/03/03 Keiichi Ishioka (version 0.1)
%2016/12/23 Keiichi Ishioka (version 0.2)
%   correct errors in the manuals of LVTSZG, LVTSWG, LVTGZS, and LVTGWS.
%2017/08/30 Keiichi Ishioka (version 0.3)
%   correct errors in the manuals of LVTSZG, LVTGZS.
%
\documentclass[a4paper]{scrartcl}


\title{Manual of LVPACK}
\author{}
\date{}

\begin{document}

\maketitle

\section{Outline}

This is a package of subroutines to compute the associated
Legendre function transform.

Let $m$ and $n$ be the zonal and the total wavenumbers of
the associated Legendre function, respectively, and 
let the truncation wavenumbers for them be $M$ and $N$,
respectively. Here, $N\ge M$ is assumed (when $N=M$, it corresponds
to the triangular truncation).
Using this notation, the associated Legendre function transform
under these truncation wavenumbers is written as follows.
\begin{equation}
G^m(\varphi)\equiv\sum^N_{n=|m|}s^m_nP^m_n(\sin\varphi).
\end{equation}
Here, $\varphi$ is the latitude and
$P^m_n(\mu)$ is the associated Legendre function normalized to 2,
which is defined as:
\begin{equation}
P^m_n(\mu)\equiv\sqrt{(2n+1)\frac{(n-|m|)!}{(n+|m|)!}}
\frac1{2^nn!}(1-\mu^2)^{|m|/2}
\frac{d^{n+|m|}}{d\mu^{n+|m|}}(\mu^2-1)^n,
\end{equation}
\begin{equation}
\int^1_{-1}\{P^m_n(\mu)\}^2d\mu=2.
\end{equation}

The forward associated Legendre function transform
is written as follows.
\begin{equation}
s^m_n=\frac12\int^{\pi/2}_{-\pi/2}G^m(\varphi)P^m_n(\sin\varphi)\cos\varphi
d\varphi.
\end{equation}

In numerical calculation, the forward associated Legendre function 
transform is approximated as follows by using the Gauss-Legendre
integration formula,
\begin{equation}
s^m_n=\frac12\sum^J_{j=1}w_jG^m(\varphi_j)P^m_n(\sin\varphi_j).
\end{equation}
Here, $\varphi_j$s are called Gaussian latitude, which is defined as
the $J$ zero points (sorted ascending order) of the Legendre
polynomial $P_J(\sin\varphi)$ (this corresponds to 
the associated Legendre function of $m=0$ without the normalization
factor (the factor in $\sqrt{\quad}$), and $w_j$ is 
the Gaussian weight corresponding to each Gaussian latitude, 
which is defined as,
\begin{equation}
w_j\equiv\frac{2(1-\mu_j^2)}{\{JP_{J-1}(\mu_j)\}^2}.
\end{equation}
Here, $\mu_j\equiv\sin\varphi_j$.
In a certain condition, this integration formula gives the 
perfect approximation or the same value as the original integration.

This subroutine package contains
subroutines that do backward transform of spectral data ($s^m_n$) 
to wave data for each latitude ($G^m(\varphi_j)$),
subroutines that do forward transform of
wave data for each latitude ($G^m(\varphi_j)$)
to spectral data ($s^m_n$), and ancillary subroutines.

Here, $\varphi_j$s are $J$ Gaussian latutudes defined above.
In the following explanations for subroutines, the notations below
are used.
\begin{center}
\begin{tabular}{ll}
\texttt{MM}:& $M$(the truncation number for $m$)\\
\texttt{NN}:& $N$(the truncation number for $n$)\\
\texttt{NM}:& maximum value of $N$ to be used\\
\texttt{JM}:& $J$(the number of Gaussioan latitudes)\\
\texttt{N}:& $n$(the total wavenumber)\\
\texttt{M}:& $m$(the zonal wavenumber)\\
\texttt{J}:& $j$(the index for each Gaussian latitude)
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{List of subroutines}

\vspace{1em}
\begin{tabular}{ll}
\texttt{LVINIT} & Initialzation\\
\texttt{LVTSZG} & Transform from spectral data to wave data (zonal componet)\\
\texttt{LVTGZS} & Transform from wave data to spectral data (zonal componet)\\
\texttt{LVTSWG} & Transform from spectral data to wave data (wave componet)\\
\texttt{LVTGWS} & Transform from wave data to spectral data (wave componet)
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Usage of each subroutine}

\subsection{LVINIT}

\begin{enumerate}

\item Purpose

Initialization routine for \texttt{LVPACK}.
It initializes the arrays, \texttt{P, R, JC},
which are used other subroutines in \texttt{LVPACK}

\item Definition

\item Synopsis 
    
\texttt{LVINIT(MM,NM,JM,P,R,JC,WP)}
  
\item Parameters 
    
\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$(the truncation 
  number for $n$) \\
& & to be used.\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & Output. An array which is used in other routines in LVPACK.\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & Output. An array which is used in other routines in LVPACK.\\
\texttt{JC}  & \multicolumn{2}{l}{\texttt{(I(MM*(2*NM-MM-1)/8+MM))}} \\
 &    & Output. An array which is used in other routines in LVPACK.\\
\texttt{WP}  & \texttt{(D(JM/2*MM))} 
      & Working area.
\end{tabular}

\item Remark

(a) \texttt{MM} must be a non-negative integer.

\texttt{JM} must be an even number ($\ge 2$).
Maximum efficiency is achieved by setting \texttt{JM} to be 
a multiple of 8 when you do make ISPACK with setting either
SSE=avx or SSE=fma.
When setting SSE=avx512, maximum efficiency is achieved
by setting 
\texttt{JM} to be a multiple of 16.

\texttt{NM} must satisfy 
\texttt{NM} $\ge$ \texttt{MM}.

(b) While using \texttt{LVPACK}, the arrays \texttt{P, R, JC}
must not be changed.

(c) When \texttt{P} is declared as \texttt{P(JM/2,5+2*MM)},
   \texttt{P(J,1)}:  $\sin(\varphi_{J/2+j})$,
   \texttt{P(J,2)}:  $\frac12 w_{J/2+j}$, 
   \texttt{P(J,3)}:  $\cos(\varphi_{J/2+j})$,
   \texttt{P(J,4)}:  $1/\cos(\varphi_{J/2+j})$,
are contained.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{LVTSZG}

\begin{enumerate}

\item Purpose 

Transform from spectral data to wave data (zonal componet).

\item Definition

For the zonal component, transform spectral data ($s^0_n$) 
to wave data ($G^0(\varphi_j)$) at each latitude
by the backward associated Legendre function transform (see Outline).

\item Synopsis 

\texttt{LVTSZG(MM,NM,NN,JM,S,G,P,Q,R,WS,IPOW)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$ to be used\\
\texttt{NN} & \texttt{(I)} & Input. $N$
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM} must hold)\\
\texttt{JM} & \texttt{(I)} & Input. $J$\\
\texttt{S} & \texttt{(D(NN+1))} & Input. Array that contains $s^0_n$\\
\texttt{G} & \texttt{(D(JM))} & Output. Array to contain $G^0(\varphi_j)$\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & Input. Array initialized by LVINIT\\
\texttt{Q} & \texttt{(D(JM/2*5))} & Working area\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2)}}\\
 &     & Input. Array initialized by LVINIT\\\\
\texttt{WS} & \texttt{(D((NN+1))} & Working area\\
\texttt{IPOW} & \texttt{(I)} & Input. 
The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) If \texttt{S} and \texttt{G} are declared as 
\texttt{S(0:NN)} and \texttt{G(JM)}, \texttt{S(N)} should 
contain $s^0_n$. In that case, $G^0(\varphi_j)$
is to be contained in \texttt{G(J)}.

(b) The array \texttt{G} must be aligned with 32byte boundary
if you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs and setting \texttt{JM} to be a multiple of 8.
The array \texttt{G} must be aligned with 64byte boundary
if you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs and setting \texttt{JM} to be a multiple of 16.

(c) If you set \texttt{IPOW}$=l$, 
$(\cos\varphi_j)^{-l}G^0(\varphi_j)$ is returned 
in the output array \texttt{G} instead of $G^0(\varphi_j)$.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{LVTGZS}

\begin{enumerate}

\item Purpose 

Transform from wave data to spectral data (zonal componet).


\item Definition

For the zonal component, transform wave data ($G^0(\varphi_j)$) 
at each latitude
to spectral data ($s^0_n$) 
by the forward associated Legendre function transform (see Outline).

\item Synopsis 

\texttt{LVTGZS(MM,NM,NN,JM,S,G,P,Q,R,WS,IPOW)}

  
\item Parameters

(Since most of parameters are the same as in LVTSZG,
the following explanation is only for parameters
different from those in LVTSZG,)

\begin{tabular}{lll}
\texttt{S} & \texttt{(D((NN+1))} & Output Array to contain $s^0_n$\\
\texttt{G} & \texttt{(D(JM))} & Input. Array that 
contains $G^0(\varphi_j)$\\
\texttt{IPOW} & \texttt{(I)} & Input. 
The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) If \texttt{S} and \texttt{G} are declared as 
\texttt{S(0:NN)} and \texttt{G(JM)}, 
\texttt{G(J)} should 
contain $G^0(\varphi_j)$.
In that case, $s^0_n$ is to 
be contained in \texttt{S(N)}.

(b) If you set \texttt{IPOW}$=l$, 
$(\cos\varphi_j)^{-l}G^0(\varphi_j)$ is used as the input
instead of $G^0(\varphi_j)$.
   
\end{enumerate}

%---------------------------------------------------------------------

\subsection{LVTSWG}

\begin{enumerate}

\item Purpose 

Transform from spectral data to wave data (wave componet).

\item Definition

For the zonal component, transform spectral data ($s^m_n$) 
to wave data ($G^m(\varphi_j)$) at each latitude
by the backward associated Legendre function transform (see Outline).

\item Synopsis 

\texttt{LVTSWG(MM,NM,NN,JM,M,S,G,P,Q,R,JC,WS,IPOW)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$ to be used\\
\texttt{NN} & \texttt{(I)} & Input. $N$
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM} must hold)\\
\texttt{JM} & \texttt{(I)} & Input. $J$\\
\texttt{M} & \texttt{(I)} & Input. zonal wavenumber $m$ 
(\texttt{M}$\ge$ 0 must hold)\\
\texttt{S} & \texttt{(D((NN-M+1)*2))} & Input. Array that contains $s^m_n$\\
\texttt{G} & \texttt{(D(JM*2))} & Output. Array to contain $G^m(\varphi_j)$\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & Input. Array initialized by LVINIT\\
\texttt{Q} & \texttt{(D(JM/2*7))} & Working area\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2)}}\\
 &     & Input. Array initialized by LVINIT\\
\texttt{JC}  & \texttt{(I(MM*(2*NM-MM-1)/8+MM))}  & Input. Array initialized by LVINIT\\
\texttt{WS} & \texttt{(D((NN-M+1)*2))} & Working area\\
\texttt{IPOW} & \texttt{(I)} & Input. 
The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) If \texttt{S} and \texttt{G} are declared as 
\texttt{S(2,0:NN)} and \texttt{G(2,JM)}, \texttt{S(1,N)} 
and \texttt{S(2,N)} should 
contain $\mbox{Re}(s^m_n)$ and $\mbox{Im}(s^m_n)$, respectively.
In that case, $\mbox{Re}(G^m(\varphi_j))$ and 
$\mbox{Im}(G^m(\varphi_j))$ 
are to be contained in \texttt{G(1,J)} and \texttt{G(2,J)}, respectively.

(b) The array \texttt{G} must be aligned with 32byte boundary
if you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs and setting \texttt{JM} to be a multiple of 8.
The array \texttt{G} must be aligned with 64byte boundary
if you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs and setting \texttt{JM} to be a multiple of 16.

(c) If you set \texttt{IPOW}$=l$, 
$(\cos\varphi_j)^{-l}G^m(\varphi_j)$ is returned 
in the output array \texttt{G} instead of $G^m(\varphi_j)$.

\end{enumerate}


%---------------------------------------------------------------------

\subsection{LVTGWS}

\begin{enumerate}

\item Purpose 

Transform from wave data to spectral data (wave componet).

\item Definition

For wave component, transform wave data ($G^m(\varphi_j)$) 
at each latitude
to spectral data ($s^m_n$) 
by the forward associated Legendre function transform (see Outline).

\item Synopsis 

\texttt{LVTGWS(MM,NM,NN,JM,M,S,G,P,Q,R,JC,WS,IPOW)}

  
\item Parameters

(Since most of parameters are the same as in LVTSWG,
the following explanation is only for parameters
different from those in LVTSWG,)

\begin{tabular}{lll}
\texttt{S} & \texttt{(D((NN-M+1)*2))} & Output. Array to contain $s^m_n$\\
\texttt{G} & \texttt{(D(JM*2))} & Input. Array that 
contains $G^m(\varphi_j)$\\
\texttt{IPOW} & \texttt{(I)} & Input. 
The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) If \texttt{S} and \texttt{G} are declared as 
\texttt{S(2,0:NN)} and \texttt{G(2,JM)}, \texttt{G(1,J)} and \texttt{G(2,J)}should 
contain $\mbox{Re}(G^m(\varphi_j))$ and 
$\mbox{Im}(G^m(\varphi_j))$, respectively.
In that case,  $\mbox{Re}(s^m_n)$ and $\mbox{Im}(s^m_n)$
are to be contained in \texttt{S(1,N)} and \texttt{S(2,N)}, respectively.

(b) If you set \texttt{IPOW}$=l$, 
$(\cos\varphi_j)^{-l}G^m(\varphi_j)$ is used as the input
instead of $G^m(\varphi_j)$.
   
\end{enumerate}



\end{document}
