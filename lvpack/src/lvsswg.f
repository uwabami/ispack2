************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVSSWG(NM,NN,JM,JV,JR,M,S,G,P,PM,Q,R,JC,WS,IPOW,IFLAG)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,5),PM(JM/2,2)
      DIMENSION R(*)
      DIMENSION S(2,M:NN)
      DIMENSION WS(2,M:NN)
      DIMENSION Q(JV,0:6,JR),G(JM*2),JC(*)

      IE=0
      IA=IE+(NM-M)/2*2
      IC=IA+(NM-M+1)/2

      IJ=0

      DO L=1,(NN+1-M)/2
        N=2*L-1+M
        WS(1,N)=R(IA+L)*S(1,N)
        WS(2,N)=R(IA+L)*S(2,N)
      END DO

      IF(NN-M.LE.1) THEN
        N=M
        WS(1,N)=S(1,N)
        WS(2,N)=S(2,N)
      ELSE
        N=M
        WS(1,N)=S(1,N)+R(IE+N-M+1)*S(1,N+2)
        WS(2,N)=S(2,N)+R(IE+N-M+1)*S(2,N+2)
        DO N=M+2,NN-2,2
          WS(1,N)=R(IE+N-M)*S(1,N)+R(IE+N-M+1)*S(1,N+2) 
          WS(2,N)=R(IE+N-M)*S(2,N)+R(IE+N-M+1)*S(2,N+2) 
        END DO
        WS(1,N)=R(IE+N-M)*S(1,N)
        WS(2,N)=R(IE+N-M)*S(2,N)
      END IF

      DO ID=1,JM/(2*JV*JR)

        L=0
        DO IR=1,JR
          JD=IR+JR*(ID-1)
          DO J=1,JV
            Q(J,0,IR)=P(J+JV*(JD-1),5)
          END DO
          JCT=MIN(JV,JC(IJ+1)-JV*(JD-1))
          DO J=1,JCT
            Q(J,1,IR)=PM(J+JV*(JD-1),1)
            Q(J,2,IR)=PM(J+JV*(JD-1),2)
          END DO
          DO J=MAX(1,JCT+1),JV
            Q(J,1,IR)=0
            Q(J,2,IR)=0
          END DO
        END DO

        DO IR=1,JR
          CALL LVSET0(JV*4,Q(1,3,IR))
        END DO

        LD=1
        JC1=JC(IJ+LD)
        JB=MIN(JR,(JC1-1-JV*JR*(ID-1)+JV)/JV)

        DO N=M,NN-6,4
          L=(N-M)/2
          LD=L/2+1
          JC1=JC(IJ+LD)
          JB=MIN(JR,(JC1-1-JV*JR*(ID-1)+JV)/JV)
          IF(JB.GE.1) THEN
            IF(JV.EQ.4) THEN
              CALL LVQSWG(JB,R(IC+2*L+3),WS(1,N),Q)
            ELSE IF(JV.EQ.8) THEN
              CALL LVOSWG(JB,R(IC+2*L+3),WS(1,N),Q)
            ELSE
              CALL LVLSWG(JV,JB,R(IC+2*L+3),WS(1,N),Q)
            END IF
          END IF
          JC1=JC(IJ+LD)
          JC2=JC(IJ+LD+1)
          IF(JC2.GT.JC1) THEN
            ID1=(JC1-1)/(JV*JR)+1
            ID2=(JC2-1)/(JV*JR)+1
            JB=MIN(JR,(JC2-1-JV*JR*(ID-1)+JV)/JV)
            IF(ID.GE.ID1.AND.ID.LE.ID2) THEN
              JS=MAX(1,(JC1-1-(ID-1)*JV*JR)/JV+1)
              JE=MIN(JR,(JC2-1-(ID-1)*JV*JR)/JV+1)
              DO IR=JS,JE
                JD=IR+JR*(ID-1)
                JCT1=MAX(0,JC1-JV*(JD-1))
                JCT2=MIN(JV,JC2-JV*(JD-1))
                DO J=JCT1+1,JCT2
                  Q(J,1,IR)=PM(J+JV*(JD-1),1)
                  Q(J,2,IR)=PM(J+JV*(JD-1),2)
                END DO
              END DO
            END IF
          END IF
        END DO

        IF(JB.GE.1) THEN
          L=(N-M)/2
          IF(NN-N.EQ.0) THEN
            CALL LV0SWG(JV,JB,WS(1,N),Q)
          ELSE IF(NN-N.EQ.1) THEN
            CALL LV1SWG(JV,JB,WS(1,N),Q)
          ELSE IF(NN-N.EQ.2) THEN
            CALL LV2SWG(JV,JB,WS(1,N),Q)
          ELSE IF(NN-N.EQ.3) THEN
            CALL LV3SWG(JV,JB,WS(1,N),Q)
          ELSE IF(NN-N.EQ.4) THEN
            CALL LV4SWG(JV,JB,R(IC+2*L+3),WS(1,N),Q)
          ELSE IF(NN-N.EQ.5) THEN
            CALL LV5SWG(JV,JB,R(IC+2*L+3),WS(1,N),Q)
          END IF
        END IF

        IF(IPOW.EQ.0) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)
                GQ2=Q(J,5,IR)*P(J+JVD,1)
                G(J   +JM+JVD*2)= GQ1+Q(J,4,IR)
                G(J+JV+JM+JVD*2)= GQ2+Q(J,6,IR)
                G(JV+1-J   +JM-JD*JV*2)=-GQ1+Q(J,4,IR)
                G(JV+1-J+JV+JM-JD*JV*2)=-GQ2+Q(J,6,IR)
              END DO
            END DO
          ELSE                  ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)
                GQ2=Q(J,5,IR)*P(J+JVD,1)
                G(1+2*(JM/2+J+JVD-1))=GQ1+Q(J,4,IR)
                G(2+2*(JM/2+J+JVD-1))=GQ2+Q(J,6,IR)
                G(1+2*(JM/2+1-(J+JVD)-1))=-GQ1+Q(J,4,IR)
                G(2+2*(JM/2+1-(J+JVD)-1))=-GQ2+Q(J,6,IR)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.1) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)
                GQ2=Q(J,5,IR)*P(J+JVD,1)*P(J+JVD,4)
                G(J   +JM+JVD*2)= GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(J+JV+JM+JVD*2)= GQ2+Q(J,6,IR)*P(J+JVD,4)
                G(JV+1-J   +JM-JD*JV*2)=-GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(JV+1-J+JV+JM-JD*JV*2)=-GQ2+Q(J,6,IR)*P(J+JVD,4)
              END DO
            END DO
          ELSE                  ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)              
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)
                GQ2=Q(J,5,IR)*P(J+JVD,1)*P(J+JVD,4)
                G(1+2*(JM/2+J+JVD-1))=GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(2+2*(JM/2+J+JVD-1))=GQ2+Q(J,6,IR)*P(J+JVD,4)
                G(1+2*(JM/2+1-(J+JVD)-1))=-GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(2+2*(JM/2+1-(J+JVD)-1))=-GQ2+Q(J,6,IR)*P(J+JVD,4)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.2) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)*P(J+JVD,4)
                GQ2=Q(J,5,IR)*P(J+JVD,1)*P(J+JVD,4)*P(J+JVD,4)
                G(J   +JM+JVD*2)
     &              = GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(J+JV+JM+JVD*2)
     &              = GQ2+Q(J,6,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(JV+1-J   +JM-JD*JV*2)
     &              =-GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(JV+1-J+JV+JM-JD*JV*2)
     &              =-GQ2+Q(J,6,IR)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          ELSE                  ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)*P(J+JVD,4)
                GQ2=Q(J,5,IR)*P(J+JVD,1)*P(J+JVD,4)*P(J+JVD,4)
                G(1+2*(JM/2+J+JVD-1))
     &              =GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(2+2*(JM/2+J+JVD-1))
     &              =GQ2+Q(J,6,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(1+2*(JM/2+1-(J+JVD)-1))
     &              =-GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(2+2*(JM/2+1-(J+JVD)-1))
     &              =-GQ2+Q(J,6,IR)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          END IF
        END IF

      END DO

      END
