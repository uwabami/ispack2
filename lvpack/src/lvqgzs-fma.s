########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvqgzs_
.globl _lvqgzs_	
lvqgzs_:
_lvqgzs_:
	movl (%rdi),%edi
	shlq $5,%rdi 
	movq %rdi,%r8	
	shlq $2,%rdi
	addq %r8,%rdi
	addq %rcx,%rdi

	vpxor %ymm0,%ymm0,%ymm0
	vpxor %ymm1,%ymm1,%ymm1
	vpxor %ymm2,%ymm2,%ymm2
	vpxor %ymm3,%ymm3,%ymm3

	vbroadcastsd   (%rsi),%ymm8
	vbroadcastsd  8(%rsi),%ymm9
	vbroadcastsd 16(%rsi),%ymm10
	vbroadcastsd 24(%rsi),%ymm11

L00:
	vmovapd   (%rcx),%ymm5
	vmovapd   %ymm5,%ymm4
	vmovapd 32(%rcx),%ymm12
	vmovapd 64(%rcx),%ymm13

	vfmadd213pd %ymm9,%ymm8,%ymm5
	vfmadd213pd %ymm11,%ymm10,%ymm4
	vfmadd213pd %ymm12,%ymm13,%ymm5
	vfmadd213pd %ymm13,%ymm5,%ymm4	
	
	vmovapd 96(%rcx),%ymm15
	vfmadd231pd %ymm12,%ymm15,%ymm1
	vfmadd231pd %ymm13,%ymm15,%ymm3
	
	vmovapd 128(%rcx),%ymm15
	vfmadd231pd %ymm12,%ymm15,%ymm0
	vfmadd231pd %ymm13,%ymm15,%ymm2

	vmovapd %ymm5,32(%rcx)
	vmovapd %ymm4,64(%rcx)
	
	addq $160,%rcx
	cmpq %rcx,%rdi
	jne L00

	vhaddpd %ymm0,%ymm0,%ymm0
	vhaddpd %ymm1,%ymm1,%ymm1
	vhaddpd %ymm2,%ymm2,%ymm2
	vhaddpd %ymm3,%ymm3,%ymm3
	vextractf128 $1,%ymm0,%xmm8
	vextractf128 $1,%ymm1,%xmm9
	vextractf128 $1,%ymm2,%xmm10
	vextractf128 $1,%ymm3,%xmm11
	vaddsd   (%rdx),%xmm8,%xmm8
	vaddsd  8(%rdx),%xmm9,%xmm9
	vaddsd 16(%rdx),%xmm10,%xmm10
	vaddsd 24(%rdx),%xmm11,%xmm11
	vaddsd %xmm8,%xmm0,%xmm0
	vaddsd %xmm9,%xmm1,%xmm1
	vaddsd %xmm10,%xmm2,%xmm2
	vaddsd %xmm11,%xmm3,%xmm3
	vmovsd %xmm0,  (%rdx)
	vmovsd %xmm1, 8(%rdx)
	vmovsd %xmm2,16(%rdx)			
	vmovsd %xmm3,24(%rdx)

	ret
