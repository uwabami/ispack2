************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVSGZS(NM,NN,JM,JV,JR,S,G,P,Q,R,WS,IPOW,IFLAG)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,*)
      DIMENSION R(*)
      DIMENSION S(0:NN)
      DIMENSION WS(0:NN) ! 使わないけど..
      DIMENSION Q(JV,0:4,JR),G(*)

      M=0

      IE=0
      IA=IE+(NM-M)/2*2
      IC=IA+(NM-M+1)/2

      CALL LVSET0((NN+1-M),S)

      DO ID=1,JM/(2*JV*JR)

        L=0
        DO IR=1,JR
          JD=IR+JR*(ID-1)
          DO J=1,JV
            Q(J,0,IR)=P(J+JV*(JD-1),5)
            Q(J,1,IR)=1
            Q(J,2,IR)=R(IC+2*L+1)*P(J+JV*(JD-1),5)+R(IC+2*L+2)
          END DO
        END DO

        IF(IPOW.EQ.0) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(J+JM+JVD*2)+G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,2)
                Q(J,3,IR)=(G(J+JM+JVD*2)-G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)
              END DO
            END DO
          ELSE ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(JM/2+J+JVD)+G(JM/2+1-(J+JVD)))
     &              *P(J+JVD,2)
                Q(J,3,IR)=(G(JM/2+J+JVD)-G(JM/2+1-(J+JVD)))
     &              *P(J+JVD,1)*P(J+JVD,2)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.1) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(J+JM+JVD*2)+G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,2)*P(J+JVD,4)
                Q(J,3,IR)=(G(J+JM+JVD*2)-G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)
              END DO
            END DO
          ELSE ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(JM/2+J+JVD)+G(JM/2+1-(J+JVD)))
     &              *P(J+JVD,2)*P(J+JVD,4)
                Q(J,3,IR)=(G(JM/2+J+JVD)-G(JM/2+1-(J+JVD)))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.2) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(J+JM+JVD*2)+G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,3,IR)=(G(J+JM+JVD*2)-G(JV+1-J+JM-JD*JV*2))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          ELSE ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                Q(J,4,IR)=(G(JM/2+J+JVD)+G(JM/2+1-(J+JVD)))
     &              *P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
                Q(J,3,IR)=(G(JM/2+J+JVD)-G(JM/2+1-(J+JVD)))
     &              *P(J+JVD,1)*P(J+JVD,2)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          END IF
        END IF

        LD=1
        JB=JR

        DO N=M,NN-6,4
          L=(N-M)/2
          LD=L/2+1
          IF(JV.EQ.4) THEN
            CALL LVQGZS(JB,R(IC+2*L+3),S(N),Q)
          ELSE IF(JV.EQ.8) THEN
            CALL LVOGZS(JB,R(IC+2*L+3),S(N),Q)
          ELSE
            CALL LVLGZS(JV,JB,R(IC+2*L+3),S(N),Q)
          END IF
        END DO

        L=(N-M)/2
        IF(NN-N.EQ.0) THEN
          CALL LV0GZS(JV,JB,S(N),Q)
        ELSE IF(NN-N.EQ.1) THEN
          CALL LV1GZS(JV,JB,S(N),Q)
        ELSE IF(NN-N.EQ.2) THEN
          CALL LV2GZS(JV,JB,S(N),Q)
        ELSE IF(NN-N.EQ.3) THEN
          CALL LV3GZS(JV,JB,S(N),Q)
        ELSE IF(NN-N.EQ.4) THEN
          CALL LV4GZS(JV,JB,R(IC+2*L+3),S(N),Q)
        ELSE IF(NN-N.EQ.5) THEN
          CALL LV5GZS(JV,JB,R(IC+2*L+3),S(N),Q)
        END IF

      END DO

      DO L=1,(NN+1-M)/2
        N=2*L-1+M
        S(N)=R(IA+L)*S(N)
      END DO

      DO N=(NN-M)/2*2+M,M+2,-2
        S(N)=R(IE+N-M)*S(N)+R(IE+N-M-1)*S(N-2) 
      END DO

      END
