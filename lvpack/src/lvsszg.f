************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVSSZG(NM,NN,JM,JV,JR,S,G,P,Q,R,WS,IPOW,IFLAG)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,*)
      DIMENSION R(*)
      DIMENSION S(0:NN)
      DIMENSION WS(0:NN)
      DIMENSION Q(JV,0:4,JR),G(*)

      M=0

      IE=0
      IA=IE+(NM-M)/2*2
      IC=IA+(NM-M+1)/2

      DO L=1,(NN+1-M)/2
        N=2*L-1+M
        WS(N)=R(IA+L)*S(N)
      END DO

      IF(NN-M.LE.1) THEN
        N=M
        WS(N)=S(N)
      ELSE
        N=M
        WS(N)=S(N)+R(IE+N-M+1)*S(N+2)
        DO N=M+2,NN-2,2
          WS(N)=R(IE+N-M)*S(N)+R(IE+N-M+1)*S(N+2) 
        END DO
        WS(N)=R(IE+N-M)*S(N)
      END IF

      DO ID=1,JM/(2*JV*JR)

        L=0
        DO IR=1,JR
          JD=IR+JR*(ID-1)
          DO J=1,JV
            Q(J,0,IR)=P(J+JV*(JD-1),5)
            Q(J,1,IR)=1
            Q(J,2,IR)=R(IC+2*L+1)*P(J+JV*(JD-1),5)+R(IC+2*L+2)
          END DO
        END DO

        DO IR=1,JR
          CALL LVSET0(JV*2,Q(1,3,IR))
        END DO

        JB=JR

        DO N=M,NN-6,4
          L=(N-M)/2
          LD=L/2+1
          IF(JV.EQ.4) THEN
            CALL LVQSZG(JB,R(IC+2*L+3),WS(N),Q)
          ELSE IF(JV.EQ.8) THEN
            CALL LVOSZG(JB,R(IC+2*L+3),WS(N),Q)
          ELSE
            CALL LVLSZG(JV,JB,R(IC+2*L+3),WS(N),Q)
          END IF
        END DO

        L=(N-M)/2
        IF(NN-N.EQ.0) THEN
          CALL LV0SZG(JV,JB,WS(N),Q)
        ELSE IF(NN-N.EQ.1) THEN
          CALL LV1SZG(JV,JB,WS(N),Q)
        ELSE IF(NN-N.EQ.2) THEN
          CALL LV2SZG(JV,JB,WS(N),Q)
        ELSE IF(NN-N.EQ.3) THEN
          CALL LV3SZG(JV,JB,WS(N),Q)
        ELSE IF(NN-N.EQ.4) THEN
          CALL LV4SZG(JV,JB,R(IC+2*L+3),WS(N),Q)
        ELSE IF(NN-N.EQ.5) THEN
          CALL LV5SZG(JV,JB,R(IC+2*L+3),WS(N),Q)
        END IF

        IF(IPOW.EQ.0) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)
                G(J     +JM+JVD*2)  = GQ1+Q(J,4,IR)
                G(JV+1-J+JM-JD*JV*2)=-GQ1+Q(J,4,IR)
                G(J+JV+JM+JVD*2)=0
                G(JV+1-J+JV+JM-JD*JV*2)=0
              END DO
            END DO
          ELSE                  ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)
                G(JM/2+J+JVD)    = GQ1+Q(J,4,IR)
                G(JM/2+1-(J+JVD))=-GQ1+Q(J,4,IR)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.1) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)
                G(J     +JM+JVD*2)  = GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(JV+1-J+JM-JD*JV*2)=-GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(J+JV+JM+JVD*2)=0
                G(JV+1-J+JV+JM-JD*JV*2)=0
              END DO
            END DO
          ELSE                  ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)
                G(JM/2+J+JVD)    = GQ1+Q(J,4,IR)*P(J+JVD,4)
                G(JM/2+1-(J+JVD))=-GQ1+Q(J,4,IR)*P(J+JVD,4)
              END DO
            END DO
          END IF
        ELSE IF(IPOW.EQ.2) THEN
          IF(IFLAG.EQ.0) THEN   ! SVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)*P(J+JVD,4)
                G(J     +JM+JVD*2)  
     &              = GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(JV+1-J+JM-JD*JV*2)
     &              =-GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(J+JV+JM+JVD*2)=0
                G(JV+1-J+JV+JM-JD*JV*2)=0
              END DO
            END DO
          ELSE                  ! LVTSWG用
            DO IR=1,JR
              JD=IR+JR*(ID-1)
              JVD=JV*(JD-1)
              DO J=1,JV
                GQ1=Q(J,3,IR)*P(J+JVD,1)*P(J+JVD,4)*P(J+JVD,4)
                G(JM/2+J+JVD)    
     &              = GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
                G(JM/2+1-(J+JVD))
     &              =-GQ1+Q(J,4,IR)*P(J+JVD,4)*P(J+JVD,4)
              END DO
            END DO
          END IF


        END IF

      END DO
      
      END
