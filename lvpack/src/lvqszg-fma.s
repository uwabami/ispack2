########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvqszg_
.globl _lvqszg_	
lvqszg_:
_lvqszg_:	
	movl (%rdi),%edi
	shlq $5,%rdi 
	movq %rdi,%r8	
	shlq $2,%rdi
	addq %r8,%rdi
	addq %rcx,%rdi
	
	vbroadcastsd   (%rdx),%ymm0 
	vbroadcastsd  8(%rdx),%ymm1 
	vbroadcastsd 16(%rdx),%ymm2 
	vbroadcastsd 24(%rdx),%ymm3 

	vbroadcastsd   (%rsi),%ymm8 
	vbroadcastsd  8(%rsi),%ymm9 
	vbroadcastsd 16(%rsi),%ymm10
	vbroadcastsd 24(%rsi),%ymm11

L00:
	vmovapd   (%rcx),%ymm15
	vmovapd   %ymm15,%ymm14 
	vmovapd 32(%rcx),%ymm12 
	vmovapd 64(%rcx),%ymm13 

	vfmadd213pd %ymm9,%ymm8,%ymm15
	vfmadd213pd %ymm11,%ymm10,%ymm14
	vfmadd213pd %ymm12,%ymm13,%ymm15
	vfmadd213pd %ymm13,%ymm15,%ymm14 	
	
	vmovapd 96(%rcx),%ymm4 
	vfmadd231pd %ymm1,%ymm12,%ymm4
	vfmadd213pd 128(%rcx),%ymm0,%ymm12	
	vfmadd231pd %ymm3,%ymm13,%ymm4
	vfmadd231pd %ymm2,%ymm13,%ymm12

	vmovapd %ymm15,32(%rcx) 
	vmovapd %ymm14,64(%rcx) 
	vmovapd %ymm4,96(%rcx)
	vmovapd %ymm12,128(%rcx)

	addq $160,%rcx
	cmpq %rcx,%rdi
	jne L00

	ret
