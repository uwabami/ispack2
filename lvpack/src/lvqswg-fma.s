########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvqswg_
.globl _lvqswg_	
lvqswg_:
_lvqswg_:	
	movl (%rdi), %edi
	shlq $5,%rdi 
	movq %rdi,%r8
	shlq $3,%rdi
	subq %r8,%rdi
	addq %rcx,%rdi

	vbroadcastsd   (%rdx),%ymm0
	vbroadcastsd 16(%rdx),%ymm1
	vbroadcastsd 32(%rdx),%ymm2
	vbroadcastsd 48(%rdx),%ymm3
	
	vbroadcastsd  8(%rdx),%ymm4
	vbroadcastsd 24(%rdx),%ymm5
	vbroadcastsd 40(%rdx),%ymm6
	vbroadcastsd 56(%rdx),%ymm7

	vbroadcastsd   (%rsi),%ymm8
	vbroadcastsd  8(%rsi),%ymm9
	vbroadcastsd 16(%rsi),%ymm10
	vbroadcastsd 24(%rsi),%ymm11

L00:
	vmovapd   (%rcx),%ymm15
	vmovapd   %ymm15,%ymm14
	vmovapd 32(%rcx),%ymm12
	vmovapd 64(%rcx),%ymm13

	vfmadd213pd %ymm9,%ymm8,%ymm15
	vfmadd213pd %ymm11,%ymm10,%ymm14
	vfmadd213pd %ymm12,%ymm13,%ymm15
	vfmadd213pd %ymm13,%ymm15,%ymm14
	vmovapd %ymm15,32(%rcx)
	vmovapd %ymm14,64(%rcx)
	
	vmovapd 96(%rcx),%ymm14
	vmovapd 128(%rcx),%ymm15
	vfmadd231pd %ymm1,%ymm12,%ymm14
	vfmadd231pd %ymm0,%ymm12,%ymm15
	vfmadd231pd %ymm3,%ymm13,%ymm14
	vfmadd231pd %ymm2,%ymm13,%ymm15
	vmovapd %ymm14,96(%rcx)
	vmovapd %ymm15,128(%rcx)

	vmovapd 160(%rcx),%ymm14
	vfmadd231pd %ymm5,%ymm12,%ymm14
	vfmadd213pd 192(%rcx),%ymm4,%ymm12
	vfmadd231pd %ymm7,%ymm13,%ymm14
	vfmadd231pd %ymm6,%ymm13,%ymm12
	vmovapd %ymm14,160(%rcx)
	vmovapd %ymm12,192(%rcx)
	
	addq $224,%rcx
	cmpq %rcx,%rdi
	jne L00

	ret
