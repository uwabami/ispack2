########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvqgws_
.globl _lvqgws_	
lvqgws_:
_lvqgws_:	
	movl   (%rdi), %edi  # : JB が rdi に

	# JB: rdi
        # AC: rsi	
	# SD: rdx	
	# Q: rcx

	shlq $5,%rdi # JB*8*4 が rsi に
	movq %rdi,%r8	
	addq %r8,%r8
	addq %r8,%r8
	addq %r8,%r8	
	subq %rdi,%r8	# r8 に JB*8*4*7 が入る
	
	addq %rcx,%r8

	vzeroall
	vbroadcastsd   (%rsi),%ymm8 # AC5 
	vbroadcastsd  8(%rsi),%ymm9 # AC6 
	vbroadcastsd 16(%rsi),%ymm10 # AC7 
	vbroadcastsd 24(%rsi),%ymm11 # AC8 

	movq %rcx,%rax
#.align 64	
L00:
	vmovapd   (%rax),%ymm14 # X2	
	vmovapd 32(%rax),%ymm12 # Q1
	vmovapd 64(%rax),%ymm13 # Q2

	vmulpd %ymm14,%ymm8,%ymm15
	vaddpd %ymm15,%ymm9,%ymm15
	vmulpd %ymm15,%ymm13,%ymm15
	vaddpd %ymm15,%ymm12,%ymm15
	vmovapd %ymm15,32(%rax) # Q1
	
	vmulpd %ymm14,%ymm10,%ymm14	
	vaddpd %ymm14,%ymm11,%ymm14
	vmulpd %ymm14,%ymm15,%ymm14
	vaddpd %ymm14,%ymm13,%ymm14
	vmovapd %ymm14,64(%rax) # Q2
	
	vmovapd 96(%rax),%ymm14 # G1R
	vmulpd %ymm12,%ymm14,%ymm15 # Q1*G1R
	vaddpd %ymm1,%ymm15,%ymm1 # SD2R=SD2R+Q1*G1R
	vmulpd %ymm13,%ymm14,%ymm15 # Q2*G1R
	vaddpd %ymm3,%ymm15,%ymm3 # SD4R=SD4R+Q2*G1R

	vmovapd 128(%rax),%ymm14 # G2R	
	vmulpd %ymm12,%ymm14,%ymm15 # Q1*G2R
	vaddpd %ymm0,%ymm15,%ymm0 # SD1R=SD1R+Q1*G2R	
	vmulpd %ymm13,%ymm14,%ymm15 # Q2*G2R
	vaddpd %ymm2,%ymm15,%ymm2 # SD3R=SD3R+Q2*G2R
	
	vmovapd 160(%rax),%ymm14 # G1I
	vmulpd %ymm12,%ymm14,%ymm15 # Q1*G1I
	vaddpd %ymm5,%ymm15,%ymm5 # SD2I=SD2I+Q1*G1I	
	vmulpd %ymm13,%ymm14,%ymm15 # Q2*G1I
	vaddpd %ymm7,%ymm15,%ymm7 # SD4I=SD4I+Q2*G1I

	vmovapd 192(%rax),%ymm14 # G2I			
	vmulpd %ymm12,%ymm14,%ymm15 # Q1*G2I
	vaddpd %ymm4,%ymm15,%ymm4 # SD1I=SD1I+Q1*G2I	
	vmulpd %ymm13,%ymm14,%ymm15 # Q2*G2I
	vaddpd %ymm6,%ymm15,%ymm6 # SD3I=SD3I+Q2*G2I
	
	addq $224,%rax
	cmpq %rax,%r8
	jne L00

	vmovupd %ymm0,-32(%rsp)
	fldl (%rdx)
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl (%rdx)
	
	vmovupd %ymm1,-32(%rsp)
	fldl 16(%rdx)
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 16(%rdx)
	
	vmovupd %ymm2,-32(%rsp)
	fldl 32(%rdx)	
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 32(%rdx)
	
	vmovupd %ymm3,-32(%rsp)
	fldl 48(%rdx)
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 48(%rdx)

	vmovupd %ymm4,-32(%rsp)
	fldl 8(%rdx)
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 8(%rdx)
	
	vmovupd %ymm5,-32(%rsp)
	fldl 24(%rdx)
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 24(%rdx)
	
	vmovupd %ymm6,-32(%rsp)
	fldl 40(%rdx)	
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 40(%rdx)
	
	vmovupd %ymm7,-32(%rsp)
	fldl 56(%rdx)
        faddl -32(%rsp)
	faddl -24(%rsp)
	faddl -16(%rsp)
        faddl -8(%rsp)			
	fstpl 56(%rdx)

	ret
