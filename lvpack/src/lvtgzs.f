************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVTGZS(NM,NN,JM,S,G,P,Q,R,WS,IPOW)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,*)
      DIMENSION R(*)
      DIMENSION S(0:NN)
      DIMENSION WS(0:NN)
      DIMENSION Q(JM/2*5),G(JM)

      CALL LVGPRM(JM,JV,JR)
      CALL LVSGZS(NM,NN,JM,JV,JR,S,G,P,Q,R,WS,IPOW,1)
      
      END
