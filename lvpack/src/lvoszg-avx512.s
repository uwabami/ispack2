########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvoszg_
.globl _lvoszg_	
lvoszg_:
_lvoszg_:
	movl (%rdi),%edi
	shlq $6,%rdi
	movq %rdi,%r8
	shlq $2,%rdi
	addq %r8,%rdi
	addq %rcx,%rdi

	vbroadcastsd   (%rdx),%zmm0
	vbroadcastsd  8(%rdx),%zmm1
	vbroadcastsd 16(%rdx),%zmm2
	vbroadcastsd 24(%rdx),%zmm3

	vbroadcastsd   (%rsi),%zmm8
	vbroadcastsd  8(%rsi),%zmm9
	vbroadcastsd 16(%rsi),%zmm10
	vbroadcastsd 24(%rsi),%zmm11

L00:
	vmovapd   (%rcx),%zmm15	
	vmovapd   (%rcx),%zmm14
	vmovapd 64(%rcx),%zmm12
	vfmadd213pd %zmm9,%zmm8,%zmm15
	vmovapd 128(%rcx),%zmm13	
	vfmadd213pd %zmm11,%zmm10,%zmm14	
	vmovapd 192(%rcx),%zmm16
	vfmadd231pd %zmm1,%zmm12,%zmm16
	vfmadd213pd %zmm12,%zmm13,%zmm15
	vmovapd %zmm15,64(%rcx)
	vfmadd213pd 256(%rcx),%zmm0,%zmm12
	vfmadd231pd %zmm3,%zmm13,%zmm16
	vfmadd231pd %zmm2,%zmm13,%zmm12
	vmovapd %zmm16,192(%rcx)
	vmovapd %zmm12,256(%rcx)
	vfmadd213pd %zmm13,%zmm15,%zmm14		
	vmovapd %zmm14,128(%rcx)

	addq $320,%rcx
	cmpq %rcx,%rdi
	jne L00

	ret
