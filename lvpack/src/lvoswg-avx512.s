########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvoswg_
.globl _lvoswg_	
lvoswg_:
_lvoswg_:	
	movl (%rdi),%edi
	shlq $6,%rdi
	movq %rdi,%r8
	shlq $3,%rdi
	subq %r8,%rdi
	addq %rcx,%rdi

	vbroadcastsd   (%rdx),%zmm0
	vbroadcastsd 16(%rdx),%zmm1
	vbroadcastsd 32(%rdx),%zmm2
	vbroadcastsd 48(%rdx),%zmm3
	vbroadcastsd  8(%rdx),%zmm4
	vbroadcastsd 24(%rdx),%zmm5
	vbroadcastsd 40(%rdx),%zmm6
	vbroadcastsd 56(%rdx),%zmm7
	vbroadcastsd   (%rsi),%zmm8
	vbroadcastsd  8(%rsi),%zmm9
	vbroadcastsd 16(%rsi),%zmm10
	vbroadcastsd 24(%rsi),%zmm11
L00:
	vmovapd   (%rcx),%zmm15
	vmovapd   (%rcx),%zmm14
	vmovapd 64(%rcx),%zmm12
	vfmadd213pd %zmm9,%zmm8,%zmm15
	vmovapd 128(%rcx),%zmm13
	vfmadd213pd %zmm11,%zmm10,%zmm14
	vmovapd 192(%rcx),%zmm16
	vfmadd231pd %zmm1,%zmm12,%zmm16
	vmovapd 256(%rcx),%zmm17 
	vfmadd231pd %zmm0,%zmm12,%zmm17
	vmovapd 320(%rcx),%zmm18
	vfmadd231pd %zmm5,%zmm12,%zmm18
	vfmadd213pd %zmm12,%zmm13,%zmm15
	vmovapd %zmm15,64(%rcx)
	vfmadd213pd 384(%rcx),%zmm4,%zmm12
	vfmadd231pd %zmm3,%zmm13,%zmm16
	vmovapd %zmm16,192(%rcx)	
	vfmadd231pd %zmm2,%zmm13,%zmm17
	vmovapd %zmm17,256(%rcx)	
	vfmadd231pd %zmm7,%zmm13,%zmm18
	vmovapd %zmm18,320(%rcx)	
	vfmadd213pd %zmm13,%zmm15,%zmm14
	vmovapd %zmm14,128(%rcx)
	vfmadd231pd %zmm6,%zmm13,%zmm12
	vmovapd %zmm12,384(%rcx)	
	addq $448,%rcx
	cmpq %rcx,%rdi
	jne L00

	ret
