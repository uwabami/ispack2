########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvqszg_
.globl _lvqszg_	
lvqszg_:
_lvqszg_:	
	movl   (%rdi), %edi  # : JB が rdi に

	# JB: rdi
        # AC: rsi	
	# SD: rdx	
	# Q: rcx

	shlq $5,%rdi # JB*8*4 が rsi に
	movq %rdi,%r8	
	addq %r8,%r8
	addq %r8,%r8	
	addq %rdi,%r8	# r8 に JB*8*4*5 が入る
	
	addq %rcx,%r8

	vbroadcastsd   (%rdx),%ymm0 # SD1R を ymm0 の4箇所に
	vbroadcastsd  8(%rdx),%ymm1 # SD2R を ymm1 の4箇所に
	vbroadcastsd 16(%rdx),%ymm2 # SD3R を ymm2 の4箇所に
	vbroadcastsd 24(%rdx),%ymm3 # SD4R を ymm3 の4箇所に

	vbroadcastsd   (%rsi),%ymm8 # AC1 を ymm0 の4箇所に
	vbroadcastsd  8(%rsi),%ymm9 # AC2 を ymm1 の4箇所に
	vbroadcastsd 16(%rsi),%ymm10 # AC3 を ymm2 の4箇所に
	vbroadcastsd 24(%rsi),%ymm11 # AC4 を ymm3 の4箇所に

	movq %rcx,%rax
#.align 64	
L00:
	vmovapd   (%rax),%ymm14 # X2		
	vmovapd 32(%rax),%ymm12 # Q1
	vmovapd 64(%rax),%ymm13 # Q2

	vmulpd %ymm14,%ymm8,%ymm15
	vaddpd %ymm15,%ymm9,%ymm15
	vmulpd %ymm15,%ymm13,%ymm15
	vaddpd %ymm15,%ymm12,%ymm15
	vmovapd %ymm15,32(%rax) # Q1			

	vmulpd %ymm14,%ymm10,%ymm14
	vaddpd %ymm14,%ymm11,%ymm14
	vmulpd %ymm14,%ymm15,%ymm14
	vaddpd %ymm14,%ymm13,%ymm14
	vmovapd %ymm14,64(%rax) # Q2
	
	vmovapd 96(%rax),%ymm14 # G1R
	vmulpd %ymm12,%ymm1,%ymm15 # Q1*SD2R
	vaddpd %ymm14,%ymm15,%ymm14 # G1R+SD2R*Q1
	vmulpd %ymm13,%ymm3,%ymm15 # Q2*SD4R
	vaddpd %ymm15,%ymm14,%ymm14 # G1R
	vmovapd %ymm14,96(%rax)

	vmovapd 128(%rax),%ymm14 # G2R
	vmulpd %ymm12,%ymm0,%ymm15 # Q1*SD1R
	vaddpd %ymm14,%ymm15,%ymm14 # G2R+SD1R*Q1	
	vmulpd %ymm13,%ymm2,%ymm15 # Q2*SD3R	
	vaddpd %ymm15,%ymm14,%ymm14 # G2R
	vmovapd %ymm14,128(%rax)

	addq $160,%rax
	cmpq %rax,%r8
	jne L00

	ret
