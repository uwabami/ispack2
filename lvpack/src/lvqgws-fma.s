########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl lvqgws_
.globl _lvqgws_	
lvqgws_:
_lvqgws_:
	movl (%rdi), %edi
	shlq $5,%rdi 
	movq %rdi,%r8
	shlq $3,%rdi
	subq %r8,%rdi
	addq %rcx,%rdi

	vpxor %ymm0,%ymm0,%ymm0
	vpxor %ymm1,%ymm1,%ymm1
	vpxor %ymm2,%ymm2,%ymm2
	vpxor %ymm3,%ymm3,%ymm3
	vpxor %ymm4,%ymm4,%ymm4
	vpxor %ymm5,%ymm5,%ymm5
	vpxor %ymm6,%ymm6,%ymm6
	vpxor %ymm7,%ymm7,%ymm7
	
	vbroadcastsd   (%rsi),%ymm8 # AC5 
	vbroadcastsd  8(%rsi),%ymm9 # AC6 
	vbroadcastsd 16(%rsi),%ymm10 # AC7 
	vbroadcastsd 24(%rsi),%ymm11 # AC8 

L00:
	vmovapd   (%rcx),%ymm15
	vmovapd   %ymm15,%ymm14
	vmovapd 32(%rcx),%ymm12
	vmovapd 64(%rcx),%ymm13

	vfmadd213pd %ymm9,%ymm8,%ymm15
	vfmadd213pd %ymm12,%ymm13,%ymm15
	vfmadd213pd %ymm11,%ymm10,%ymm14
	vfmadd213pd %ymm13,%ymm15,%ymm14
	vmovapd %ymm15,32(%rcx)
	
	vmovapd 96(%rcx),%ymm15
	vfmadd231pd %ymm12,%ymm15,%ymm1
	vfmadd231pd %ymm13,%ymm15,%ymm3
	vmovapd 128(%rcx),%ymm15				
	vfmadd231pd %ymm12,%ymm15,%ymm0
	vfmadd231pd %ymm13,%ymm15,%ymm2
	
	vmovapd 160(%rcx),%ymm15
	vfmadd231pd %ymm12,%ymm15,%ymm5
	vfmadd231pd %ymm13,%ymm15,%ymm7
	vmovapd 192(%rcx),%ymm15				
	vfmadd231pd %ymm12,%ymm15,%ymm4
	vfmadd231pd %ymm13,%ymm15,%ymm6

	vmovapd %ymm14,64(%rcx)	
	
	addq $224,%rcx
	cmpq %rcx,%rdi
	jne L00

	vhaddpd %ymm0,%ymm0,%ymm0
	vhaddpd %ymm1,%ymm1,%ymm1
	vhaddpd %ymm2,%ymm2,%ymm2
	vhaddpd %ymm3,%ymm3,%ymm3
	vhaddpd %ymm4,%ymm4,%ymm4
	vhaddpd %ymm5,%ymm5,%ymm5
	vhaddpd %ymm6,%ymm6,%ymm6
	vhaddpd %ymm7,%ymm7,%ymm7
	vextractf128 $1,%ymm0,%xmm8
	vextractf128 $1,%ymm1,%xmm9
	vextractf128 $1,%ymm2,%xmm10
	vextractf128 $1,%ymm3,%xmm11
	vextractf128 $1,%ymm4,%xmm12
	vextractf128 $1,%ymm5,%xmm13
	vextractf128 $1,%ymm6,%xmm14
	vextractf128 $1,%ymm7,%xmm15	
	vaddsd   (%rdx),%xmm8,%xmm8
	vaddsd 16(%rdx),%xmm9,%xmm9
	vaddsd 32(%rdx),%xmm10,%xmm10
	vaddsd 48(%rdx),%xmm11,%xmm11
	vaddsd  8(%rdx),%xmm12,%xmm12
	vaddsd 24(%rdx),%xmm13,%xmm13
	vaddsd 40(%rdx),%xmm14,%xmm14
	vaddsd 56(%rdx),%xmm15,%xmm15
	vaddsd %xmm8,%xmm0,%xmm0
	vaddsd %xmm9,%xmm1,%xmm1
	vaddsd %xmm10,%xmm2,%xmm2
	vaddsd %xmm11,%xmm3,%xmm3
	vaddsd %xmm12,%xmm4,%xmm4
	vaddsd %xmm13,%xmm5,%xmm5
	vaddsd %xmm14,%xmm6,%xmm6
	vaddsd %xmm15,%xmm7,%xmm7
	vmovsd %xmm0,(%rdx)
	vmovsd %xmm1,16(%rdx)
	vmovsd %xmm2,32(%rdx)			
	vmovsd %xmm3,48(%rdx)
	vmovsd %xmm4, 8(%rdx)
	vmovsd %xmm5,24(%rdx)
	vmovsd %xmm6,40(%rdx)
	vmovsd %xmm7,56(%rdx)

	ret
