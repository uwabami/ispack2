************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVTSWG(NM,NN,JM,M,S,G,P,Q,R,JC,WS,IPOW)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,*)
      DIMENSION R(*)
      DIMENSION S(2,M:NN)
      DIMENSION WS(2,M:NN)
      DIMENSION Q(JM/2*7),G(2,JM),JC(*)

      CALL LVGPRM(JM,JV,JR)
      IE=(M*(2*NM-M)+1)/4*3+M*(2*NM-M+1)/2+1
      IJ=(M-1)*(2*NM-M)/8+M
      CALL LVSSWG(NM,NN,JM,JV,JR,M,S,G,P,P(1,4+M*2),Q,R(IE),JC(IJ),
     &    WS,IPOW,1)
      
      END
