************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE LVINIT(MM,NM,JM,P,R,JC,WP)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION P(JM/2,5+2*MM),WP(JM/2,MM)
*      DIMENSION R(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2)
      DIMENSION R(*)
      DIMENSION JC(MM*(2*NM-MM-1)/8+MM)

      CALL LVINIZ(MM,NM,JM,P,R)

!$omp parallel private(IE,IJ)
!$omp do schedule(dynamic)
      DO M=1,MM
        IE=(M*(2*NM-M)+1)/4*3+M*(2*NM-M+1)/2        
        IJ=(M-1)*(2*NM-M)/8+M-1
        CALL LVINIW(MM,NM,JM,M,P,P(1,5+2*M-1),R(IE+1),JC(IJ+1),WP(1,M))
      END DO
!$omp end do
!$omp end parallel

      END
