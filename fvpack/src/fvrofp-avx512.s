########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvrofp_
.globl _fvrofp_	
fvrofp_:
_fvrofp_:	
	movl   (%rdi), %edi  # N が rdi に	
      # X の先頭アドレスは rsi に
      # T の先頭アドレスは rdx に

	shlq $1,%rdi # rdi に 2*N	

	vbroadcastsd C2(%rip), %zmm11 # 倍精度不動小数点の   2 を zmm11 の4箇所に
	vbroadcastsd C05(%rip),%zmm12 # 倍精度不動小数点の 0.5 を zmm12 の4箇所に

        cvtsi2sd  %edi, %xmm0		
	movsd C2(%rip),%xmm1
	divsd %xmm0, %xmm1
	movsd %xmm1,-8(%rsp)	
	vbroadcastsd -8(%rsp),%zmm13

	shlq $4,%rdi # rdi に 32*N		
	
	vmovapd   (%rsi), %zmm0 # X(I,1,0)
	vmovapd 64(%rsi), %zmm1 # X(I,2,0)

#-- scaling --
	vmulpd %zmm13,%zmm0,%zmm0
	vmulpd %zmm13,%zmm1,%zmm1
#-------------
	
	vaddpd %zmm1,%zmm0,%zmm2
	vsubpd %zmm1,%zmm0,%zmm0
	vmovapd %zmm2,(%rsi)
	vmovapd %zmm0,64(%rsi)

	vmovapd (%rsi,%rdi), %zmm0 # X(I,1,N/4)	
	vmovapd 64(%rsi,%rdi), %zmm1 # X(I,2,N/4)

#-- scaling --
	vmulpd %zmm13,%zmm0,%zmm0
	vmulpd %zmm13,%zmm1,%zmm1
#-------------

	vsubpd %zmm2,%zmm2,%zmm2
	vsubpd %zmm1,%zmm2,%zmm2
	vmovapd %zmm0,  (%rsi,%rdi)  # X(I,1,N/4)	
	vmovapd %zmm2,64(%rsi,%rdi)  # X(I,2,N/4)

	cmpq $128,%rdi
	je LE # N=4 の場合はこれで終了

	shrq $3,%rdi # rdi に 8*N	
	movq %rdx,%r8
        addq %rdi,%r8	# r8 は終了判定に使う

	shlq $4,%rdi # rdi に 64*N
	addq %rsi,%rdi # rdi に rsi + 64*N

	addq $16,%rdx
	addq $128,%rsi
	subq $128,%rdi

#-- scaling ファクターを 0.5 にまとめてしまう.
	vmulpd %zmm13,%zmm12,%zmm12
#----	
	
L1:
	vbroadcastsd (%rdx), %zmm8 # T(1,K)
	vbroadcastsd 8(%rdx), %zmm9 # T(2,K)
	
	vmovapd   (%rsi), %zmm0 # X(I,1,K)
	vmovapd 64(%rsi), %zmm1 # X(I,2,K)	
	vmovapd   (%rdi), %zmm2 # X(I,1,N/2-K)
	vmovapd 64(%rdi), %zmm3 # X(I,2,N/2-K)


	vmulpd %zmm12,%zmm0,%zmm0
	vmovapd %zmm0,%zmm4	
	vfmsub231pd %zmm12,%zmm2,%zmm0 # zmm0 = zmm2 * zmm12 - zmm0	
	vfmadd231pd %zmm12,%zmm2,%zmm4 # zmm4 = zmm2 * zmm12 + zmm4
	
	vmulpd %zmm12,%zmm1,%zmm1
	vmovapd %zmm1,%zmm5	
	vfmsub231pd %zmm12,%zmm3,%zmm1 # zmm1 = zmm3 * zmm12 - zmm1
	vfmadd231pd %zmm12,%zmm3,%zmm5 # zmm5 = zmm3 * zmm12 + zmm5

	vmovapd %zmm0,%zmm2
	vfnmadd132pd %zmm9,%zmm4,%zmm2 # zmm2 = - zmm2 * zmm9 + zmm4
	vfmadd231pd %zmm8,%zmm5,%zmm2 # zmm2 = zmm5 * zmm8 + zmm2
	vmovapd %zmm2,  (%rsi) # X(I,1,K)

	vfnmadd132pd %zmm9,%zmm1,%zmm5 # zmm5 = - zmm5 * zmm9 + zmm1
	
	vfnmadd231pd %zmm8,%zmm0,%zmm5 # zmm5 = - zmm0 * zmm8 + zmm5
	vmovapd %zmm5,  64(%rsi) # X(I,2,K)

	vfmsub132pd %zmm11,%zmm2,%zmm4 # zmm4 = zmm4 * zmm11 - zmm2
	vmovapd %zmm4,(%rdi) # X(I,1,N/2-K)	

	vfnmadd132pd %zmm11,%zmm5,%zmm1 # zmm1 = - zmm1 * zmm11 + zmm5
	vmovapd %zmm1,64(%rdi) # X(I,2,N/2-K)	

	addq $16,%rdx
	addq $128,%rsi
	subq $128,%rdi
	cmpq %rdx,%r8
	jne L1
	
LE:
	
	ret

C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
C05: # 倍精度不動小数点の 0.5
	.long   0x00000000,0x3fe00000
	
