########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzof2_
.globl _fvzof2_	
fvzof2_:
_fvzof2_:	
	movl (%rdi), %edi  # NH が rdi に
      # X の先頭アドレスは rsi

	vbroadcastsd C2(%rip),%zmm11 # 倍精度不動小数点の 2 を zmm11 の4箇所に

	shlq $3,%rdi # rdi に NH*8 が	
        cvtsi2sd  %edi, %xmm0	

	movsd C2(%rip),%xmm1
	divsd %xmm0, %xmm1
	movsd %xmm1,-8(%rsp)	
	vbroadcastsd -8(%rsp),%zmm12 

	shlq $4,%rdi # rdi に NH*128 が
	
	movq %rdi,%rax
	addq %rsi,%rax
	
L1:
	vmovapd   (%rsi),      %zmm0 # 0R
	vmovapd 64(%rsi),      %zmm1 # 0I	
	vmovapd   (%rsi,%rdi), %zmm2 # 1R
	vmovapd 64(%rsi,%rdi), %zmm3 # 1I

#-- scaling --
	vmulpd %zmm12,%zmm2,%zmm2
	vmulpd %zmm12,%zmm3,%zmm3
#-------------	

	vfmadd213pd %zmm2,%zmm12,%zmm0 # zmm0 = zmm0 * zmm12 + zmm2	
	vfmadd213pd %zmm3,%zmm12,%zmm1 # zmm1 = zmm1 * zmm12 + zmm3
	
	vfnmadd213pd %zmm0,%zmm11,%zmm2 # zmm2 = - zmm2 * zmm11 + zmm0	
	vfnmadd213pd %zmm1,%zmm11,%zmm3 # zmm3 = - zmm3 * zmm11 + zmm1	

	vmovapd %zmm0,   (%rsi)
	vmovapd %zmm1, 64(%rsi)
	vmovapd %zmm2,   (%rsi,%rdi)
	vmovapd %zmm3, 64(%rsi,%rdi)
#-----
	addq $128,%rsi
	cmpq %rsi,%rax
	jne L1

	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
