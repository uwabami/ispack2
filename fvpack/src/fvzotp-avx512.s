########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzotp_
.globl _fvzotp_	
fvzotp_:
_fvzotp_:	
      # X の先頭アドレスは rdi に
      # IT の先頭アドレスは rsi に

#	vzeroupper

	movl (%rsi),%ecx

	cmpq $0,%rcx
	je LE
	
	shlq $3,%rcx
	lea (%rsi,%rcx),%r8

#.align 16
L0:
	movl  8(%rsi),%eax
	movl 12(%rsi),%edx
	shlq $7,%rax
	shlq $7,%rdx	
	vmovapd   (%rdi,%rax), %zmm0 
	vmovapd 64(%rdi,%rax), %zmm1 
	vmovapd   (%rdi,%rdx), %zmm2
	vmovapd 64(%rdi,%rdx), %zmm3 
	vmovapd %zmm0,  (%rdi,%rdx)
	vmovapd %zmm1,64(%rdi,%rdx)
	vmovapd %zmm2,  (%rdi,%rax)
	vmovapd %zmm3,64(%rdi,%rax) 
	addq $8,%rsi
	cmpq %r8,%rsi
	jne L0

LE:	
       
	ret
