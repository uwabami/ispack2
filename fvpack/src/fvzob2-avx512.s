########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl fvzob2_
.globl _fvzob2_
fvzob2_:
_fvzob2_:	
	movl (%rdi), %edi  # NH が rdi に
      # X の先頭アドレスは rsi

	vbroadcastsd C2(%rip),%zmm11 # 倍精度不動小数点の 2 を zmm11 の4箇所に

	shlq $7,%rdi # rdi に NH*128 が
	movq %rdi,%rax
	addq %rsi,%rax
	
L1:
	vmovapd   (%rsi),      %zmm0 # 0R
	vmovapd 64(%rsi),      %zmm1 # 0I	
	vmovapd   (%rsi,%rdi), %zmm2 # 1R
	vmovapd 64(%rsi,%rdi), %zmm3 # 1I

	vaddpd %zmm2,%zmm0,%zmm0
	vaddpd %zmm3,%zmm1,%zmm1
	
	vfnmadd213pd %zmm0,%zmm11,%zmm2 # zmm2 = - zmm2 * zmm11 + zmm0
	vfnmadd213pd %zmm1,%zmm11,%zmm3 # zmm3 = - zmm3 * zmm11 + zmm1

	vmovapd %zmm0,   (%rsi)
	vmovapd %zmm1, 64(%rsi)
	vmovapd %zmm2,   (%rsi,%rdi)
	vmovapd %zmm3, 64(%rsi,%rdi)
#-----
	addq $128,%rsi
	cmpq %rsi,%rax
	jne L1

	ret
       
C2: # 倍精度不動小数点の 2
	.long   0x00000000,0x40000000
	
