************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
************************************************************************
      SUBROUTINE FVRTFA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION X(*),IT(*),T(*)

      IF(M.EQ.4) THEN
        CALL FVRQFA(N,X,IT,T)
      ELSE IF(M.EQ.8) THEN
        CALL FVROFA(N,X,IT,T)
      ELSE
        CALL FVRMFA(M,N,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVRTBA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION X(*),IT(*),T(*)

      IF(M.EQ.4) THEN
        CALL FVRQBA(N,X,IT,T)
      ELSE IF(M.EQ.8) THEN
        CALL FVROBA(N,X,IT,T)
      ELSE
        CALL FVRMBA(M,N,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVRMBA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION T(N/2,2),IT(N/2)
      DIMENSION X(M,2,0:N-1)

      IF(N.EQ.2) THEN
        CALL FVRMB2(M,X)
      ELSE
        CALL FVRMBP(M,N,X,T(1,2))
        CALL FVZMBA(M,N/2,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVRMFA(M,N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      DIMENSION T(N/2,2),IT(N/2)
      DIMENSION X(M,2,0:N-1)

      IF(N.EQ.2) THEN
        CALL FVRMF2(M,X)
      ELSE
        CALL FVZMBA(M,N/2,X,IT,T)
        CALL FVRMFP(M,N,X,T(1,2))
      END IF

      END
************************************************************************
      SUBROUTINE FVRQBA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=4)
      DIMENSION T(N/2,2),IT(N/2)
      DIMENSION X(M,2,0:N-1)

      IF(N.EQ.2) THEN
        CALL FVRQB2(X)
      ELSE
        CALL FVRQBP(N,X,T(1,2))
        CALL FVZQBA(N/2,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVRQFA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=4)
      DIMENSION T(N/2,2),IT(N/2)
      DIMENSION X(M,2,0:N-1)

      IF(N.EQ.2) THEN
        CALL FVRQF2(X)
      ELSE
        CALL FVZQBA(N/2,X,IT,T)
        CALL FVRQFP(N,X,T(1,2))
      END IF

      END
************************************************************************
      SUBROUTINE FVROBA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=8)
      DIMENSION T(N/2,2),IT(N/2)
      DIMENSION X(M,2,0:N-1)

      IF(N.EQ.2) THEN
        CALL FVROB2(X)
      ELSE
        CALL FVROBP(N,X,T(1,2))
        CALL FVZOBA(N/2,X,IT,T)
      END IF

      END
************************************************************************
      SUBROUTINE FVROFA(N,X,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)      
      PARAMETER(M=8)
      DIMENSION T(N/2,2),IT(N/2)
      DIMENSION X(M,2,0:N-1)

      IF(N.EQ.2) THEN
        CALL FVROF2(X)
      ELSE
        CALL FVZOBA(N/2,X,IT,T)
        CALL FVROFP(N,X,T(1,2))
      END IF

      END
************************************************************************
      SUBROUTINE FVRINI(N,IT,T)

      IMPLICIT REAL*8(A-H,O-Z)
      DIMENSION T(2,0:N/4-1,2),IT(N/2) 
      REAL*16 PI
      PI=4*ATAN(1Q0)


      IF(N.LT.2) THEN
        print *,'fvpack error: N.NE.2**P'
        STOP
      ELSE IF(MOD(N,2).NE.0) THEN
        print *,'fvpack error: N.NE.2**P'
        STOP
      ELSE IF(N.EQ.2) THEN
        RETURN
      END IF

      CALL FVZINI(N/2,IT,T(1,0,1))

      DO I=0,N/4-1
        T(1,I,2)=COS(2*PI*I/N)
        T(2,I,2)=SIN(2*PI*I/N)        
      END DO

      END
