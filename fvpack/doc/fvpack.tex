%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%
% last modified 2016/12/23
%
\documentclass[a4paper]{scrartcl}

\title{Manual of FVPACK}
\author{}
\date{}

\begin{document}

\maketitle

\section{Outline}

This is a package of subroutines to compute FFT.
Since the radix of the transform is 2 (and 4),  
the data-length to be transformed must be a power of two.

\vspace{1em}
In each group of subroutines shown below, the initialization
subroutine, the name of which ends with the letter \texttt{I},
must be called once before other subroutines in the group are
called. 

\vspace{1em}
For computational efficiency, the transform subroutines treat
multiple data streams simultaneously. If a two-dimensional
array, \texttt{X(I,J),I=1,2,$\cdots$,M, J=1,2,$\cdots$,N}, is
given, Fourier transform of \texttt{X(I,1),X(I,2),$\cdots$,X(I,N)}
is computed for each \texttt{I}. That is, Fourier transform
of length-\texttt{N} data stream is computed \texttt{M} times.
If only one data stream is given, you should set as \texttt{M=1}.

\vspace{1em}
If you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs and using \texttt{M=4}, the array that contains
the data streams to be transformed must be aligned with 32byte
boundary.
If you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs and using \texttt{M=8}, the array that contains
the data streams to be transformed must be aligned with 64byte
boundary.
In those cases, you should allocate a memory area with
\texttt{MLALLC}, for example.

\section{List of subroutines}

 Discrete complex Fourier transform

  \vspace{1em}
  \begin{tabular}{p{7cm}p{10cm}}
    \texttt{FVZINI(N,IT,T)} & Initialization\\
    \texttt{FVZTFA(M,N,X,IT,T)} & Forward transform\\
    \texttt{FVZTBA(M,N,X,IT,T)} & Backward transform
  \end{tabular}

 \vspace{1em}
 Discrete real Fourier transform

  \vspace{1em}
  \begin{tabular}{p{7cm}p{10cm}}
    \texttt{FVRINI(N,IT,T)} & Initialization\\
    \texttt{FVRFA(M,N,X,IT,T)} & Forward transform\\
    \texttt{FVRBA(M,N,X,IT,T)} & Backward transform
  \end{tabular}
    
\section{Usage of each subroutine}


\subsection{FVZINI/FVZTFA/FVZTBA}
\begin{enumerate}
  \item Purpose 
  \begin{quote}
    
   Discrete complex (forward/backward) Fourier transforms of $M$ sets of 
  one-dimensional data streams of length $N$, 
$\{x_j\}$ or $\{\alpha_k\}$ are computed using FFT algorithm.
Note that $N$ must be a power of two.
     \texttt{FVZINI} does initialization;
     \texttt{FVZTFA} computes forward Fourier transform;
     \texttt{FVZTBA} computes backward Fourier transform.

  \end{quote}
  \item Definition
    \begin{itemize}
     \item Forward Fourier transform
      \begin{quote}
       The input $\{x_j\}$ is transformed as follows to give 
       the output $\{\alpha_k\}$.
       \[
       \alpha_k=\frac1N\sum^{N-1}_{j=0}x_j\exp(-2\pi i\frac{jk}N),
       \quad (k=0,1,\cdots,N-1)
        \]
      \end{quote}
     \item Backward Fourier transform
      \begin{quote}
       The input $\{\alpha_k\}$ is transformed as follows to give 
       the output $\{x_j\}$.
       \[
      x_j=\sum^{N-1}_{k=0}\alpha_k\exp(2\pi i\frac{jk}N),
       \quad (j=0,1,\cdots,N-1)
      \]
      \end{quote}
     \end{itemize}    
  \item Synopsis 
  \begin{quote}
    \texttt{FVZINI(N,IT,T)}\\
    \texttt{FVZTFA(M,N,X,IT,T)}\\
    \texttt{FVZTBA(M,N,X,IT,T)}
  \end{quote}
  \item Parameters 
  \begin{quote}
    \begin{tabular}{llp{10cm}}
      \texttt{M}   & \texttt{(I)} & Input. The number of data streams $M$
       to be transformed simultaneously.\\
      \texttt{N}   & \texttt{(I)} & Input. The length of data stream $N$.\\
      \texttt{X}   & \texttt{(D)} & Input. $\{x_j\}$ or $\{\alpha_k\}$\\
                 &           & Output. $\{\alpha_k\}$ or $\{x_j\}$\\
                 &           & 3-dimensional array of 
      \texttt{M}$\times$\texttt{N}$\times$2\\
      \texttt{IT}   & \texttt{(I)} & Output(\texttt{FVZINI}) or 
Input(\texttt{FVZTFA},\texttt{FVZTBA}).\\
   & & One-dimensional array of size \texttt{N}.\\
      \texttt{T}   & \texttt{(D)} & Outupt(\texttt{FVZINI}) or 
Input(\texttt{FVZTFA},\texttt{FVZTBA}).\\
& & One-dimensional array of size \texttt{N}.
    \end{tabular}
  \end{quote}

  \item Order of data stream

  \begin{quote}

  If the input/output array is declared as \texttt{X(M,2,0:N-1)},
  the data is contained in it for each $I$ as follows.

    \begin{tabular}{|c|c|c|c|c|c|c|}\hline
     \tt X(I,1,0) & \tt X(I,2,0) & \tt X(I,1,1) & \tt X(I,2,1) & 
     $\cdots$ & \tt X(I,1,N-1) & \tt X(I,2,N-1) \\\hline\hline
      \mbox{Re}($x_0$) & \mbox{Im}($x_0$) & \mbox{Re}($x_1$) & \mbox{Im}($x_1$) &
     $\cdots$ & \mbox{Re}($x_{N-1}$) & \mbox{Im}($x_{N-1}$) \\\hline
      \mbox{Re}($\alpha_0$) & \mbox{Im}($\alpha_0$) & \mbox{Re}($\alpha_1$) & \mbox{Im}($\alpha_1$) &
     $\cdots$ & \mbox{Re}($\alpha_{N-1}$) & \mbox{Im}($\alpha_{N-1}$) \\\hline
    \end{tabular}
  \end{quote}

\end{enumerate}

\subsection{FVRINI/FVRTFA/FVRTBA}
\begin{enumerate}
  \item Purpose 
  \begin{quote}
   Discrete real (forward/backward) Fourier transforms of $M$ sets of 
  one-dimensional data streams of length $N$, 
$\{x_j\}$ are computed using FFT algorithm.
Note that $N$ must be a power of two larger than 2.
     \texttt{FVRINI} does initialization;
     \texttt{FVRTFA} computes forward Fourier transform;
     \texttt{FVRTBA} computes backward Fourier transform.

  \end{quote}
  \item Definition
    \begin{itemize}
     \item Forward Fourier transform
      \begin{quote}
       The input $\{x_j\}$ is transformed as follows to give 
       the output $\{a_k\},\{b_k\}$.
       \[
       a_k= \frac1N\sum^{N-1}_{j=0}x_j\cos\frac{2\pi jk}N,
       \quad (k=0,1,\cdots,N/2)
       \]
       \[
       b_k=-\frac1N\sum^{N-1}_{j=0}x_j\sin\frac{2\pi jk}N,
       \quad (k=1,2,\cdots,N/2-1)
       \]
      \end{quote}
     \item Backward Fourier transform
      \begin{quote}
       The input $\{a_k\},\{b_k\}$ is transformed as follows to give 
       the output $\{x_j\}$.
       \[
       x_j=a_0+a_{N/2}(-1)^j+2\sum^{N/2-1}_{k=1}(a_k\cos\frac{2\pi jk}N
                          -b_k\sin\frac{2\pi jk}N)
       \quad (j=0,1,\cdots,N-1)
      \]
      \end{quote}
     \end{itemize}    
  \item Synopsis 
  \begin{quote}
    \texttt{FVRINI(N,IT,T)}\\
    \texttt{FVRTFA(M,N,X,IT,T)}\\
    \texttt{FVRTBA(M,N,X,IT,T)}
  \end{quote}
  \item Parameters 
  \begin{quote}
    \begin{tabular}{llp{10cm}}
      \texttt{M}&\texttt{(I)}& Input. The number of data
      streams $M$\\
       & & to be transformed simultaneously.\\
      \texttt{N}&\texttt{(I)}& Input. The length of data stream $N$.\\
      \texttt{X} & \texttt{(D)} & Input. $\{x_j\}$ or $\{a_k\},\{b_k\}$\\
                 &   & Output. $\{a_k\},\{b_k\}$ or $\{x_j\}$.\\
       &      & 2-dimensional array of \texttt{M}$\times$\texttt{N}.\\
      \texttt{IT}   & \texttt{(I)} & Output(\texttt{FVRINI}) or 
               Input(\texttt{FVRTFA},\texttt{FVRTBA}).\\
   & & One-dimensional array of size \texttt{N/2}.\\
      \texttt{T}   & \texttt{(D)} & Outupt(\texttt{FVRINI}) or 
    Input(\texttt{FVRTFA},\texttt{FVRTBA}).\\
  & & One-dimensional array of size \texttt{N}.
    \end{tabular}
  \end{quote}

  \item Order of data stream
  \begin{quote}

  If the input/output array is declared as \texttt{X(M,0:N-1)},
  the data is contained in it for each $I$ as follows.

    \begin{tabular}{|c|c|c|c|c|c|c|}\hline
     \tt X(I,0) & \tt X(I,1) & \tt X(I,2) & \tt X(I,3) & 
     $\cdots$ & \tt X(I,N-2) & \tt X(I,N-1) \\\hline\hline
      $x_0$ & $x_1$ & $x_2$ & $x_3$ &
     $\cdots$ & $x_{N-2}$ & $x_{N-1}$ \\\hline
      $a_0$ & $a_{N/2}$ & $a_1$ & $b_1$ &
     $\cdots$ & $a_{N/2-1}$ & $b_{N/2-1}$ \\\hline
    \end{tabular}

  \end{quote}

  
\end{enumerate}


\end{document}
