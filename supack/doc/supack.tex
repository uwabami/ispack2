%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%
% last modified 2016/12/23
%
\documentclass[a4paper,fleqn]{scrartcl}

\usepackage{amsmath,amssymb}

\title{Manual of SUPACK}
\author{}
\date{}

\begin{document}

\maketitle

\section{Outline}

This is an MPI-parallelized version of SVPACK, which is 
a package of subroutines to compute the spectral (spherical
harmonics) transform. 
Hence, for basic definitions of the transform, see the
manual of SVPACK. 
This package is an upper-level package based on SVPACK.

Since MPI subroutines are called in this subroutine package
internally, you must call subroutines belonging to this pacakge
inside the region between \texttt{MPI\_INIT} and \texttt{MPI\_FINALIZE}.
The header file (mpif.h) must also be included as a matter of course.
For these general usages of MPI, refer to MPI manual on your system.

\subsection{About the parallelization}

The MPI-parallelization used in SUPACK is a standard one,
in which the spectral data is divided in the direction of 
the zonal wavenumber and the grid data is divided in the
direction of the latitude. The divided data are stored and 
treated in each process.
Subroutines which gives information about what zonal wavenumbers
and latitudes of spectral data and grid data, respectively are stored
in each process are provided. 
The OpenMP-parallelization for each process is also implemented.

\textbf{In the following explanations for subroutines, the number
of MPI processes is denoted by an integer-type variable, 
\texttt{NP}.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{List of subroutines}

\vspace{1em}
\begin{tabular}{ll}
\texttt{SUINIT} & Initialzation\\
\texttt{SUNM2L} & Compute the position where spectral data are stored\\
\texttt{SUL2NM} & Inverse computation of \texttt{SUNM2L}\\
\texttt{SUTS2G} & Transform from spectral data to grid data\\
\texttt{SUTG2S} & Transform from grid data to spectral data\\
\texttt{SUQRNM} & Inquire the zonal wavenumber treated by the process\\
\texttt{SUQRNJ} & Inquire the Gaussian latitude treated by the process\\
\texttt{SUGS2S} & Gather scattered spectral data\\
\texttt{SUSS2S} & Scatter gathered spectral data\\
\texttt{SUGG2G} & Gather scattered grid data\\
\texttt{SUSG2G} & Scatter gathered grid data\\
\texttt{SUINIC} & Initialize an array used in \texttt{SUCS2Y}, 
   \texttt{SUCY2S}, \textit{etc}\\
\texttt{SUCS2Y} & Transform of spectral data 
corresponding to latitudinal derivative\\
\texttt{SUCY2S} & Transform of spectral data \\
& corresponding to forward spectral transform with latitudinal derivative\\
\texttt{SUCS2X} & Transform of spectral data 
corresponding to longitudinal derivative\\
\texttt{SUINID} & Initialize arrays used in \texttt{SUCLAP}\\
\texttt{SUCLAP} & Transform of spectral data \\
& corresponding to operating Laplacian or its inverse\\
\texttt{SUCRUP} & Repacking spectral data from \\
& the triangular truncation ($N=M$) to another truncation ($N>M$)\\
\texttt{SUCRDN} & Inverse operation of \texttt{SUCRUP}
\end{tabular}

%---------------------------------------------------------------------
\section{Usage of each subroutine}

\subsection{SUINIT}

\begin{enumerate}

\item Purpose

Initialization routine for \texttt{SUPACK}.
It initializes the arrays, \texttt{IT, T, P, R, JC},
which are used other subroutines in \texttt{SUPACK}

\item Definition

\item Synopsis 
    
\texttt{SUINIT(MM,NM,IM,JM,IT,T,P,R,JC,WP)}
  
\item Parameters 
    
\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$(the truncation 
  number for $n$) \\
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{IT}  & \texttt{(I(IM/2))}    & Output. An array which is used in other routines in SUPACK.\\
\texttt{T}   & \texttt{(D(IM))} & Output. An array which is used in other routines in SUPACK.\\
\texttt{P}  & \multicolumn{2}{l}{\texttt{(D(JM/2,5+2*(MM/NP+1)))}}\\
&      & Output. An array which is used in other routines in SUPACK.\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D((5*(MM/NP+1)*(2*NM-MM/NP*NP)/4)))}}\\
 &      & Output. An array which is used in other routines in SUPACK.\\
\texttt{JC}  & \multicolumn{2}{l}{\texttt{\texttt{(I((5*(MM/NP+1)*(2*NM-MM/NP*NP+8)/8)))}}}\\
& & Output. An array which is used in other routines in SUPACK.\\
\texttt{WP}  & \texttt{(D(JM/2*MM))} 
      & Working area.
\end{tabular}

\item Remark

(a) \texttt{MM} must be a natural number.
     
\texttt{IM} must be a power of two which satisfies
\texttt{IM > 2*MM}.

\texttt{NM} must satisfy 
\texttt{NM} $\ge$ \texttt{MM}.

(b) While using \texttt{SUPACK}, the arrays \texttt{IT, T, P, R, JC}
must not be changed.

(c) When \texttt{P} is declared as \texttt{P(JM/2,5+2*(MM/NP+1))}
   \texttt{P(J,1)}:  $\sin(\varphi_{J/2+j})$,
   \texttt{P(J,2)}:  $\frac12 w_{J/2+j}$, 
   \texttt{P(J,3)}:  $\cos(\varphi_{J/2+j})$,
   \texttt{P(J,4)}:  $1/\cos(\varphi_{J/2+j})$,
are contained (for explanation for the Gaussian latitude, see SVPACK).

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUNM2L}

\begin{enumerate}

\item Purpose 

Compute the position where spectral data are stored
and the process number from the total wavenumber and the zonal wavenumber.

\item Definition

In SUPACK, spectral data are divided by multi processes.
This subroutine computes 
the position in the array ($s^m_n$)
where spectral data are stored and the process number
from the total wavenumber($n$) and the zonal wavenumber($m$).

\item Synopsis 
    
\texttt{SUNM2L(NN,N,M,IP,L)}
  
\item Parameters 

\begin{tabular}{lll}
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{N} & \texttt{(I)} & Input. $n$(the total wavenumber)\\
\texttt{M} & \texttt{(I)} & Input. $m$(the zonal wavenumber. See Remark)\\
\texttt{IP} & \texttt{(I)} & Output. The process number that treats
the data.\\
\texttt{L} & \texttt{(I)} & Output. the position where the spectral
data is stored.
\end{tabular}

\item Remark

If \texttt{M} $>$ 0, it returns the position where
$\mbox{Re}(s^m_n)$ is stored with setting 
$m=$ \texttt{M} and $n=$ \texttt{N}.
If \texttt{M} $<$ 0, it returns the position where
$\mbox{Im}(s^m_n)$ is stored with setting 
$m=$ \texttt{-M} and $n=$ \texttt{N}.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUL2NM}

\begin{enumerate}

\item Purpose 

Inverse computation of \texttt{SUNM2L}.
That is, 
it computes 
the total wavenumber and the zonal wavenumber from
the position where spectral data are stored in each process.

\item Definition

See \texttt{SUNM2L}.

\item Synopsis 
    
\texttt{SUL2NM(MM,NN,L,N,M)}
  
\item Parameters 

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{L} & \texttt{(I)} & Input. The position where the spectral
data are stored.\\
\texttt{N} & \texttt{(I)} & Output. $n$(the total wavenumber)\\
\texttt{M} & \texttt{(I)} & Output. $m$(the zonal wavenumber)\\
\end{tabular}

\item Remark

(a)  The meaning of the sign of \texttt{M} is the same as in \texttt{SUNM2L}.

(b) If the process does not treat spectral data or the 
  position specified by \texttt{L} does not contain 
  spectral data, \texttt{N=-1} and \texttt{M=0} are returned 
  as output.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUTS2G}

\begin{enumerate}

\item Purpose 

Transform from spectral data to grid data.

\item Definition

Transform scattered spectral data
to scatterd grid data
by the backward spectral transform.

\item Synopsis 

\texttt{SUTS2G(MM,NM,NN,IM,JM,JV,S,G,IT,T,P,R,JC,WS,W,IPOW)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$ to be used\\
\texttt{NN} & \texttt{(I)} & Input. $N$
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM} must hold)\\
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{JV} & \texttt{(I)} & Input. Vector length for computation\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D(((MM/NP+1)*(2*(NN+1)-MM/NP*NP)))}}\\
&  & Input. 
Array that contains spectral data\\
\texttt{G} &
\multicolumn{2}{l}{\texttt{(D(IM*((JM/JV-1)/NP+1)*JV))}}\\
& & Output. 
Array to contain grid data\\
\texttt{IT}  & \texttt{(I(IM/2))}    & Input. Array initialized by SUINIT\\
\texttt{T}   & \texttt{(D(IM))} & Input. Array initialized by SUINIT\\
\texttt{P}  & \texttt{(D(JM/2,5+2*(MM/NP+1)))} 
      & Input. Array initialized by SUINIT\\
\texttt{R}  
 & \multicolumn{2}{l}{\texttt{(D((5*(MM/NP+1)*(2*NM-MM/NP*NP)/4)))
}}\\
& &  Input. Array initialized by SUINIT\\
\texttt{JC}  & \multicolumn{2}{l}{\texttt{(I((5*(MM/NP+1)*(2*NM-MM/NP*NP+8)/8)))}}\\
& & Input. Array initialized by SUINIT\\
\texttt{WS} & \texttt{(D((NN+1)*2,MM/NP+1))} & Working area\\
\texttt{W} & 
\multicolumn{2}{l}{\texttt{(D(4*2*JV*((JM/JV-1)/NP+1)*((IM/2-1)/NP+1)*NP))}}\\
 & & Working area\\
\texttt{IPOW} & \texttt{(I)} & Input. The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) \texttt{JV} must be a divisor of \texttt{JM/2}.
If you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs,
setting \texttt{JV} to be 4 yields computational efficiancy.
If you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs,
setting \texttt{JV} to be 8 yields computational efficiancy.

(b) 
The array \texttt{W} must be aligned with 32byte boundary
if you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs and setting \texttt{JV} to be 4.
The array \texttt{W} must be aligned with 64byte boundary
if you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs and setting \texttt{JV} to be 8.

(c) If \texttt{G} is declared as 
\texttt{G(0:IM-1,((JM/JV-1)/NP+1)*JV)},
    $g(\lambda_i,\varphi_j)$ is to be contained in \texttt{G(I,J-J1+1)}.
(\texttt{J} is between \texttt{J1} and\texttt{J2}. Here, 
\texttt{J1} and \texttt{J2} are given by \texttt{SUQRNJ},
which are the lower end and the upper end of \texttt{J} treated 
by the process, respectively).

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUTG2S}

\begin{enumerate}

\item Purpose 

Transform from grid data to spectral data.

\item Definition

Transform scattered grid data
to scatterd spectral data
by the forward spectral transform.

\item Synopsis 

\texttt{SUTG2S(MM,NM,NN,IM,JM,JV,S,G,IT,T,P,R,JC,WS,W,IPOW)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$ to be used\\
\texttt{NN} & \texttt{(I)} & Input. $N$
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM} must hold)\\
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{JV} & \texttt{(I)} & Input. Vector length for computation\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D(((MM/NP+1)*(2*(NN+1)-MM/NP*NP)))}}\\
&  & Output. 
Array to contain spectral data\\
\texttt{G} &
\multicolumn{2}{l}{\texttt{(D(IM*((JM/JV-1)/NP+1)*JV))}}\\
& & Input. 
Array that contains grid data\\
\texttt{IT}  & \texttt{(I(IM/2))}    & Input. Array initialized by SUINIT\\
\texttt{T}   & \texttt{(D(IM))} & Input. Array initialized by SUINIT\\
\texttt{P}  & \texttt{(D(JM/2,5+2*(MM/NP+1)))} 
      & Input. Array initialized by SUINIT\\
\texttt{R}  
 & \multicolumn{2}{l}{\texttt{(D((5*(MM/NP+1)*(2*NM-MM/NP*NP)/4)))
}}\\
& &  Input. Array initialized by SUINIT\\
\texttt{JC}  & \multicolumn{2}{l}{\texttt{(I((5*(MM/NP+1)*(2*NM-MM/NP*NP+8)/8)))}}\\
& & Input. Array initialized by SUINIT\\
\texttt{WS} & \texttt{(D((NN+1)*2,MM/NP+1))} & Working area\\
\texttt{W} & 
\multicolumn{2}{l}{\texttt{(D(4*2*JV*((JM/JV-1)/NP+1)*((IM/2-1)/NP+1)*NP))}}\\
 & & Working area\\
\texttt{IPOW} & \texttt{(I)} & Input. The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) \texttt{JV} must be a divisor of \texttt{JM/2}.
If you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs,
setting \texttt{JV} to be 4 yields computational efficiancy.
If you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs,
setting \texttt{JV} to be 8 yields computational efficiancy.

(b) 
The array \texttt{W} must be aligned with 32byte boundary
if you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs and setting \texttt{JV} to be 4.
The array \texttt{W} must be aligned with 64byte boundary
if you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs and setting \texttt{JV} to be 8.

(c) If \texttt{G} is declared as 
\texttt{G(0:IM-1,((JM/JV-1)/NP+1)*JV)},
    $g(\lambda_i,\varphi_j)$ should be contained in \texttt{G(I,J-J1+1)}.
(\texttt{J} is between \texttt{J1} and\texttt{J2}. Here, 
\texttt{J1} and \texttt{J2} are given by \texttt{SUQRNJ},
which are the lower end and the upper end of \texttt{J} treated 
by the process, respectively).

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUQRNM}

\begin{enumerate}

\item Purpose 

Inquire the zonal wavenumber treated by the process

\item Definition

Return the zonal wavenumber of spectral data treated by the process.

\item Synopsis 
    
\texttt{SUQRNM(MM,MCM,MC)}
  
\item Parameters 

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{MCM} & \texttt{(I)} & Output. The number of $m$s treated by the
process\\
\texttt{MC} & \texttt{(I(MM/NP+1))} & Output. Array to contain
$m$s treated by the process
\end{tabular}

\item Remark

(a) If the process does not treat spectral data, 
\texttt{MCM=0} is returned as output.

(b) In the array \texttt{MCM(1:MCM)},
the list of $m$s is returned.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUQRNJ}

\begin{enumerate}

\item Purpose 

Inquire the Gaussian latitude treated by the process

\item Definition

Return the Gaussian latitude of grid data treated by the process.

\item Synopsis 
    
\texttt{SUQRNJ(JM,JV,J1,J2)}
  
\item Parameters 

\begin{tabular}{lll}
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{JV} & \texttt{(I)} & Input. Vector length for computation\\
\texttt{J1} & \texttt{(I)} & Output. Lower end of $j$ treated by the process\\
\texttt{J2} & \texttt{(I)} & Output. Upper end of $j$ treated by the process
\end{tabular}

\item Remark

(a) If the process does not treat grid data, 
\texttt{J1=0} and \texttt{J2=-1}
\texttt{MCM=0} are returned as output.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUGS2S}

\begin{enumerate}

\item Purpose 

Gather scattered spectral data.

\item Definition

Gather spectral data cattered in each process to process 0.

\item Synopsis 

\texttt{SUGS2S(MM,NN,S,SALL,SBUF)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{S} & \multicolumn{2}{l}{\texttt{
    (D(((MM/NP+1)*(2*(NN+1)-MM/NP*NP))))}}\\
& & Input. Array that contains scattered spectral data\\
\texttt{SALL} & \texttt{(D((MM+1)*(MM+1)))}
 &  Output. Array to contain gathered spectral data\\
& & (for process 0 only)\\
\texttt{SBUF} & \multicolumn{2}{l}{\texttt{
  (D(MM/NP+1)*(2*(NN+1)-MM/NP*NP)*NP)}}\\
& & Working area
\end{tabular}

\item Remark

(a) This subroutine must be called by all processes, but
the memory regions for \texttt{SALL} and \texttt{SBUF}
should be allocated in process 0 only.

\end{enumerate}


%---------------------------------------------------------------------

\subsection{SUSS2S}

\begin{enumerate}

\item Purpose 

Scatter gathered spectral data.

\item Definition

Scatter spectral data progathered in process 0 to each process.

\item Synopsis 

\texttt{SUSS2S(MM,NN,SALL,S,SBUF)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{SALL} & \texttt{(D((MM+1)*(MM+1)))}
 &  Input. Array that contains gathered spectral data\\
& & (for process 0 only)\\
\texttt{S} & \multicolumn{2}{l}{\texttt{
    (D(((MM/NP+1)*(2*(NN+1)-MM/NP*NP))))}}\\
& & Output. Array to contain scattered spectral data\\
\texttt{SBUF} & \multicolumn{2}{l}{\texttt{
  (D(MM/NP+1)*(2*(NN+1)-MM/NP*NP)*NP)}}\\
& & Working area
\end{tabular}

\item Remark

(a) This subroutine must be called by all processes, but
the memory regions for \texttt{SALL} and \texttt{SBUF}
should be allocated in process 0 only.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUGG2G}

\begin{enumerate}

\item Purpose 

Gather scattered grid data.

\item Definition

Gather grid data scattered in each process to process 0.

\item Synopsis 

\texttt{SUGG2G(IM,JM,JV,G,GALL,GBUF)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{JV} & \texttt{(I)} & Input. Vector length for computation\\
\texttt{G} & \multicolumn{2}{l}{\texttt{
    (D(IM*((JM/JV-1)/NP+1)*JV))}}\\
& & Input. Array that contains scattered grid data\\
\texttt{GALL} & \texttt{(D(IM*JM))}
 &  Output. Array to contain gathered grid data\\
& & (for process 0 only)\\
\texttt{GBUF} & \multicolumn{2}{l}{\texttt{
  (D(IM*JV*((JM/JV-1)/NP+1)*NP))}}\\
& & Working area
\end{tabular}

\item Remark

(a) This subroutine must be called by all processes, but
the memory regions for \texttt{GALL} and \texttt{GBUF}
should be allocated in process 0 only.

\end{enumerate}


%---------------------------------------------------------------------

\subsection{SUSG2G}

\begin{enumerate}

\item Purpose 

Scatter gathered grid data.

\item Definition

Scatter grid data gathered in process 0 to each process.


\item Synopsis 

\texttt{SUSG2G(IM,JM,JV,GALL,G,GBUF)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{JV} & \texttt{(I)} & Input. Vector length for computation\\
\texttt{GALL} & \texttt{(D(IM*JM))}
 &  Input. Array that contains gathered grid data\\
& & (for process 0 only)\\
\texttt{G} & \multicolumn{2}{l}{\texttt{
    (D(IM*((JM/JV-1)/NP+1)*JV))}}\\
& & Output. Array to contain scattered grid data\\
\texttt{GBUF} & \multicolumn{2}{l}{\texttt{
  (D(IM*JV*((JM/JV-1)/NP+1)*NP))}}\\
& & Working area
\end{tabular}

\item Remark

(a) This subroutine must be called by all processes, but
the memory regions for \texttt{GALL} and \texttt{GBUF}
should be allocated in process 0 only.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUINIC}

\begin{enumerate}

\item Purpose 

Initialize array \texttt{C} used in \texttt{SUCS2Y}, \texttt{SUCY2S}
\textit{etc}

\item Definition

See SUCS2Y and SUCY2S.

\item Synopsis 

\texttt{SUINIC(MT,C)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. 
The truncation wavenumber for the triangular truncation.\\
\texttt{C} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Output. 
Array used in \texttt{SUCS2Y}, \texttt{SUCY2S}, 
\textit{etc}.
\end{tabular}

\item Remark

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUCS2Y}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to latitudinal derivative
\item Definition

If the backward spectral transform, where the triangular
trancation of $N=M$ is assumed, is defined as,
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^M_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda},
\end{equation}
operating $\cos\varphi\frac{\partial}{\partial\varphi}$ to
$g$ yields
\begin{equation}
\cos\varphi\frac{\partial}{\partial\varphi}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^{M+1}_{n=|m|}
(s_y)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
That is, it can be represented by a backward spectral transform
with setting the trancation wavenumber of $n$ as $N=M+1$.
This subroutine computes $(s_y)^m_n$ from $s^m_n$.

\item Synopsis 

\texttt{SUCS2Y(MT,S,SY,C)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Input. Array that
contains $s^m_n$\\
\texttt{SY} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+2)-MT/NP*NP)))}}\\
& & Output. 
Array to contain  $(s_y)^m_n$\\
\texttt{C} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Input. 
Array initialized by SUINIC\\
\end{tabular}

\item Remark

(a) If you call\texttt{SUCS2Y(MT,S,SY,C)} and
    \texttt{SUTS2G(MT,NM,MT+1,IM,JM,SY,G,IT,T,P,Q,R,WS,WG,W,1)}
    continuously, the latitudinal derivative is returned in \texttt{G}.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUCY2S}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to the forward spectral transform with latitudinal derivative

\item Definition

If you want to compute the following form of modified forward
spectral transform
(it appers in computing the divergence of a vector field, for example):
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)
\left(-\cos\varphi\frac{\partial}{\partial\varphi}P^m_n(\sin\varphi)\right)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda,
\quad 
\end{equation}
(where, $m$ and $n$ are within the triangular truncation of $M$),
$s^m_n$ can be obtained from the result of 
the following normal forward spectral
transform of $g$:
\begin{equation}
(s_y)^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)
P^m_n(\sin\varphi)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\quad (n\le M+1=N).
\end{equation}
This subroutine computes $s^m_n$ from $(s_y)^m_n$.

\item Synopsis 

\texttt{SUCY2S(MT,SY,S,C)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{SY} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+2)-MT/NP*NP)))}}\\
&  & Input.
Array that contains $(s_y)^m_n$\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Output. 
Array to contain $s^m_n$\\
\texttt{C} & \texttt{(D(MT+1)*(MT+1))} & Input. Array initialized by SUINIC\\
\end{tabular}

\item Remark

(a)If you call \texttt{SUTG2S(MT,NM,MT+1,IM,JM,SY,G,IT,T,P,Q,R,WS,WG,W,1)} and
    \texttt{SUCY2S(MT,SY,S,C)} continuously, 
    the following computation that corresponds to
    the forward transform of the divergence of a vector field,
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
\frac{\partial}{\cos\varphi\partial\varphi}
\left(\cos\varphi g(\lambda,\varphi)\right)
P^m_n(\sin\varphi)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\end{equation}
is done and $s^m_n$ is returned in \texttt{S}.
Note that integration by parts is applied and \texttt{IPOW=1} here.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUCS2X}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to longitudinal derivative.

\item Definition

If the backward spectral transform, where the triangular
trancation of $N=M$ is assumed, is defined as,
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^M_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda},
\end{equation}
operating $\frac{\partial}{\partial\lambda}$ to $g$
yields
\begin{equation}
\frac{\partial}{\partial\lambda}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^{M}_{n=|m|}
(s_x)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
Here, $(s_x)^m_n=ims^m_n$.
This subroutine computes $(s_x)^m_n=ims^m_n$ from $s^m_n$.

\item Synopsis 

\texttt{SUCS2X(MT,S,SX)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Input. 
Array that contains $s^m_n$\\
\texttt{SX} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Output. 
Array to contain $(s_x)^m_n$
\end{tabular}

\item Remark

(a) The order of storing $(s_x)^m_n$ is the same as that of $s^m_n$.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUINID}

\begin{enumerate}

\item Purpose 

Initialize array \texttt{D} used in SUCLAP. 

\item Definition

See SUCLAP.

\item Synopsis 

\texttt{SUINID(MT,D)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{D} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)*2))}}\\
& & Output. 
Array used in SUCLAP
\end{tabular}

\item Remark

    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUCLAP}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to Laplacian or its inverse.

\item Definition

For the spherical harmonics expansion (here, the triangular
truncation of $N=M$ is assumed):
\begin{equation}
g(\lambda,\varphi)=\sum^M_{n=0}\sum^n_{m=-n}
s^m_nP^m_n(\sin\varphi)e^{im\lambda},
\end{equation}
operating the horizontal Laplacian:
\begin{equation}
\nabla^2\equiv
\frac{\partial^2}{\cos^2\varphi\partial\lambda^2}
+\frac{\partial}{\cos\varphi\partial\varphi}\left(\cos\varphi\frac{\partial}{\partial\varphi}\right)
\end{equation}
yields the following 
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}-n(n+1)a^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
by the nature of spherical harmonics.
Hence, 
introducing
\begin{equation}
(s_l)^m_n\equiv -n(n+1)s^m_n,
\end{equation}
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}(s_l)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
holds. 
Inversely, when
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}s^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
holds, introducing
\begin{equation}
(s_l)^m_n\equiv -\frac1{n(n+1)}s^m_n
\end{equation}
yields
\begin{equation}
g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}(s_l)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}

This subroutine computes $(s_l)^m_n = -n(n+1)s^m_n$
from $s^m_n$, or inversely computes $(s_l)^m_n = -s^m_n/(n(n+1))$
from $s^m_n$.

\item Synopsis 
    
\texttt{SUCLAP(MT,S,SL,D,IFLAG)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber \\
& & for the triangular truncation.\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Input. 
Array that contains $s^m_n$\\
\texttt{SL} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Output. 
Array to contain $(s_l)^m_n$\\
\texttt{D} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)*2))}}\\
& & Input.
	Array initialized by SUINID\\
\texttt{IFLAG} & \texttt{(I)} & Input. If \texttt{IFLAG=1}, it
operates Laplacian, \\
& & and if \texttt{IFLAG=2}, it operates the 
inverse of Laplacian.
\end{tabular}

\item Remark

(a) If \texttt{IFLAG=2}, $s^0_0$ is returned in $(s_l)^0_0$ (where $n=0$).

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUCRUP}

\begin{enumerate}

\item Purpose

Repacking spectral data from 
the triangular truncation ($N=M$) to another truncation ($N>M$)

\item Definition

It is often the case that one wants the addition of the results
of \texttt{SUCS2Y} and \texttt{SUCS2X}. However, they are different
types of spectral data because their truncation wavenumbers $N$ for
$n$ are different and so they can not be added with each other
directly. This subroutine 
repacks spectral data from ($s^m_n$) of
the triangular truncation ($N=M$) to ($(s_r)^m_n$)
of another truncation ($N>M$).
Note that zeros are returned in $(s_r)^m_n$ where $n>M$.

\item Synopsis 
    
\texttt{SUCRUP(MT,NN,S,SR)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber \\
& & for the triangular truncation.\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Input. 
Array that contains $s^m_n$\\
\texttt{SR} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(NN+1)-MT/NP*NP)))}}\\
& & Output. 
Array to contain $(s_r)^m_n$
\end{tabular}

\item Remark

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SUCRDN}

\begin{enumerate}

\item Purpose

Inverse operation of \texttt{SUCRUP}. That is, 
repacks spectral data from 
a truncation ($N>M$) to
the triangular truncation ($N=M$).

\item Definition

This subroutine 
repacks spectral data from 
($(s_r)^m_n$) of a truncation ($N>M$)
to ($s^m_n$) of
the triangular truncation ($N=M$).
Note that the data in $(s_r)^m_n$ where $n>M$ are
discarded.

\item Synopsis 
    
\texttt{SUCRDN(MT,NN,SR,S)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber \\
& & for the triangular truncation.\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{SR} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(NN+1)-MT/NP*NP)))}}\\
& & Input.
Array that contains $(s_r)^m_n$\\
\texttt{S} &
\multicolumn{2}{l}{\texttt{(D((MT/NP+1)*(2*(MT+1)-MT/NP*NP)))}}\\
& & Output. 
Array to contain $s^m_n$
\end{tabular}

\item Remark

\end{enumerate}




\end{document}
