************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
      SUBROUTINE SUGS2S(MM,NN,S,SALL,SBUF)

      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'mpif.h'
      DIMENSION S(*),SALL(*),SBUF(*)

      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NP,IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,IP,IERR)

      NB=(MM/NP+1)*(2*(NN+1)-MM/NP*NP)

      CALL MPI_GATHER(S,NB,MPI_REAL8,SBUF,NB,MPI_REAL8,
     &                 0,MPI_COMM_WORLD,IERR)

      IF(IP.EQ.0) THEN
        CALL SUCOPY(NN+1,SBUF,SALL)
        DO M=1,MM
          K=M/NP
          NS=K*(2*(NN+1)-(K-1)*NP)+1
          CALL SUNM2L(NN,M,M,IPDEST,L)
          CALL SVNM2L(NN,M,M,LA)
          CALL SUCOPY(2*(NN+1-M),SBUF(NS+NB*IPDEST),SALL(LA))
        END DO
      END IF

      END
