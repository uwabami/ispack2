************************************************************************
* ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
* Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU Lesser General Public
* License as published by the Free Software Foundation; either
* version 2.1 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Lesser General Public License for more details.
* 
* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
* 02110-1301 USA.
************************************************************************
*------------------------------------------------------------------------
*     SUPACKの初期化
*------------------------------------------------------------------------
      SUBROUTINE SUINIT(MM,NM,IM,JM,IT,T,P,R,JC,WP)
!      NP はプロセス数, IPはプロセス番号

      IMPLICIT REAL*8(A-H,O-Z)
      INCLUDE 'mpif.h'
      DIMENSION IT(IM/2),T(IM)
!      DIMENSION P(JM/2,5+2*(MM/NP+1)),WP(JM/2)
      DIMENSION P(JM/2,*),WP(JM/2)
!      DIMENSION R(5*(MM/NP+1)*(2*NM-MM/NP*NP)/4)
      DIMENSION R(*)
!      DIMENSION JC(5*(MM/NP+1)*(2*NM-MM/NP*NP+8)/8)
      DIMENSION JC(*)

      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NP,IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,IP,IERR)

      CALL FVRINI(IM,IT,T)

      CALL LVINIZ(MM,NM,JM,P,R)

      N1=(MM+1)/NP
      M1=N1*NP
      MN=N1
      IF(MOD(N1,2).EQ.0) THEN
        IF(IP.LE.MM-M1) THEN
          MN=MN+1
        END IF
      ELSE
        IF(IP.GE.NP-MM+M1-1) THEN
          MN=MN+1
        END IF
      END IF

      DO K=0,MN-1
        IE=5*K*(2*NM-NP*(K-1))/4
        IJ=K*(2*NM-NP*(K-1)+8)/8
        M=K*NP+IP+MOD(K,2)*(NP-2*IP-1)
        IF(M.NE.0) THEN
          CALL LVINIW(MM,NM,JM,M,P,P(1,5+2*K+1),R(IE+1),JC(IJ+1),WP)
        END IF
      END DO

      END
