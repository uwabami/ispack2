!***********************************************************************
! ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
! Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public
! License along with this library; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
! 02110-1301 USA.
!***********************************************************************
!***********************************************************************
!     SVPACK 内での 逆変換と正変換のテスト
!      (与えられたスペクトルデータに対して, グリッド上の勾配を求め,
!       さらにその発散に対応するスペクトルデータを計算し, 最初に与えた      
!       スペクトルデータに直接ラプラシアンを作用させたものに一致するか      
!       どうかをチェックする)                                 2016/02/02
!***********************************************************************      
      USE ISO_C_BINDING
      IMPLICIT NONE
!      INTEGER,PARAMETER :: MM=170,JM=256,IM=512
      INTEGER,PARAMETER :: MM=682,JM=1024,IM=2048
      INTEGER,PARAMETER :: NM=MM+1
      INTEGER :: N,M,L,LR,LI,LAS,ISEED
      REAL(8) :: SLMAX,RAN,SLAMAX,SL,EPS=1D-14*IM
      REAL(8),DIMENSION(:),ALLOCATABLE :: S,SD,SX,SXD,SXR,SY,SYD
      REAL(8),DIMENSION(:),ALLOCATABLE :: T,P,R,C,D,WS
      INTEGER,DIMENSION(:),ALLOCATABLE :: IT,JC
      REAL(8),DIMENSION(:),POINTER:: G,W
      TYPE(C_PTR) :: PG,PW

      ALLOCATE(S((MM+1)*(MM+1)))
      ALLOCATE(SD((MM+1)*(MM+1)))
      ALLOCATE(SX((MM+1)*(MM+1)))
      ALLOCATE(SXD((MM+1)*(MM+1)))
      ALLOCATE(SXR((MM+4)*MM+2))
      ALLOCATE(SY((MM+4)*MM+2))
      ALLOCATE(SYD((MM+1)*(MM+1)))
      ALLOCATE(T(IM))
      ALLOCATE(P(JM/2*(2*MM+5)))
      ALLOCATE(R(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))
      ALLOCATE(C((MM+1)*(MM+1)))
      ALLOCATE(D((MM+1)*(MM+1)*2))
      ALLOCATE(WS(2*(NM+1)*(MM+1)))
      ALLOCATE(IT(IM/2))
      ALLOCATE(JC(MM*(2*NM-MM-1)/8+MM))
      CALL MLALLC(PG,JM*IM)
      CALL MLALLC(PW,JM*IM*2)
      CALL C_F_POINTER(PG, G, [JM*IM])
      CALL C_F_POINTER(PW, W, [JM*IM*2])

      CALL SVINIT(MM,NM,IM,JM,IT,T,P,R,JC,W)
      CALL SVINIC(MM,C)
      CALL SVINID(MM,D)

      ISEED=1
      DO L=1,(MM+1)*(MM+1)
         CALL MLRAND(ISEED,RAN)
         S(L)=2*RAN-1
      END DO
      S(1)=0
      CALL SVCLAP(MM,S,SD,D,2)
      CALL SVCLAP(MM,SD,S,D,1)

      CALL SVCS2X(MM,SD,SX)
      CALL SVCRUP(MM,MM+1,SX,SXR)
      CALL SVCS2Y(MM,SD,SY,C)

      CALL SVTS2G(MM,NM,MM+1,IM,JM,SXR,G,IT,T,P,R,JC,WS,W,1)      
      CALL SVTG2S(MM,NM,MM+1,IM,JM,SXR,G,IT,T,P,R,JC,WS,W,1)      
      CALL SVTS2G(MM,NM,MM+1,IM,JM,SY,G,IT,T,P,R,JC,WS,W,1)      
      CALL SVTG2S(MM,NM,MM+1,IM,JM,SY,G,IT,T,P,R,JC,WS,W,1)      

      CALL SVCY2S(MM,SY,SYD,C)
      CALL SVCRDN(MM,MM+1,SXR,SX)
      CALL SVCS2X(MM,SX,SXD)

!      DO L=1,(MM+1)*(MM+1)
!         SD(L)=SXD(L)+SYD(L)
!      END DO

      SD=SXD+SYD

      SLMAX=0
      SLAMAX=0
      M=0
      DO N=0,MM
         CALL SVNM2L(MM,N,M,L)
         SL=ABS(SD(L)-S(L))
         IF(SL.GT.SLMAX) THEN
            SLMAX=SL
            LAS=L
         END IF
         SLAMAX=SLAMAX+SL**2
      END DO

      DO M=1,MM
         DO N=M,MM
            CALL SVNM2L(MM,N,M,LR)
            CALL SVNM2L(MM,N,-M,LI)
            SL=(SD(LR)-S(LR))**2+(SD(LI)-S(LI))**2
            SL=SQRT(SL)
            IF(SL.GT.SLMAX) THEN
               SLMAX=SL
               LAS=LR
            END IF
            SLAMAX=SLAMAX+SL**2
         END DO
      END DO

      CALL SVL2NM(MM,LAS,N,M)
      PRINT '(A,ES9.2,A,I5,A,I5,A)','maxerror =',SLMAX,' (n=',N,', m=',M,')'
      PRINT '(A,ES9.2)','rmserror =',SQRT(SLAMAX/((MM+1)*(MM+2)/2))
      print *,'gradient and divergence check:'
      IF(SLMAX.LE.EPS) THEN
         print *,'** OK'
      ELSE
         print *,'** Fail'
      END IF

      DEALLOCATE(S)
      DEALLOCATE(SD)
      DEALLOCATE(SX)
      DEALLOCATE(SXD)
      DEALLOCATE(SXR)
      DEALLOCATE(SY)
      DEALLOCATE(SYD)
      DEALLOCATE(T)
      DEALLOCATE(P)
      DEALLOCATE(R)
      DEALLOCATE(C)
      DEALLOCATE(D)
      DEALLOCATE(WS)
      DEALLOCATE(IT)
      DEALLOCATE(JC)
      CALL MLFREE(PG)
      CALL MLFREE(PW)
        
      END
