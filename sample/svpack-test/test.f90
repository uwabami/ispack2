!***********************************************************************
! ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
! Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public
! License along with this library; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
! 02110-1301 USA.
!***********************************************************************
      USE ISO_C_BINDING
      IMPLICIT NONE
      !      INTEGER,PARAMETER :: NTR=50
      INTEGER,PARAMETER :: NTR=1
      !      INTEGER,PARAMETER :: JM=2**11
      INTEGER,PARAMETER :: JM=2**10
      INTEGER,PARAMETER :: IM=2*JM,MM=JM-1
      INTEGER,PARAMETER :: NM=MM,NN=MM
      INTEGER,PARAMETER :: IPOW=0
      INTEGER :: ISEED,L,N,M,LR,LI,LAS,ITR,MAXTD,ICPU
      REAL(8) :: RC,RAN,SL,SLMAX,SLAMAX,GFLOPS,TIM0,TIM1
      REAL(8),DIMENSION(:),ALLOCATABLE :: S,SD,R,WS,T,P
      INTEGER(4),DIMENSION(:),ALLOCATABLE :: IT,JC
      REAL(8),DIMENSION(:),POINTER:: W,G
      TYPE(C_PTR) :: PW,PG
!$    INTEGER :: omp_get_max_threads

      ALLOCATE(S((MM+1)*(MM+1)))
      ALLOCATE(SD((MM+1)*(MM+1)))
      ALLOCATE(T(IM))
      ALLOCATE(P(JM/2*(2*MM+5)))
      ALLOCATE(R(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))
      ALLOCATE(WS(2*(NN+1)*(MM+1)))
      ALLOCATE(IT(IM/2))
      ALLOCATE(JC(MM*(2*NM-MM-1)/8+MM))
      CALL MLALLC(PG,JM*IM)
      CALL MLALLC(PW,JM*IM*2)
      CALL C_F_POINTER(PG, G, [JM*IM])
      CALL C_F_POINTER(PW, W, [JM*IM*2])

      RC=1D0*5*IM*LOG(1D0*IM)/LOG(2D0)*0.5D0*JM+1D0*(MM+1)*(MM+1)*JM
      ! ���Ѵ�/���Ѵ���1�󤢤���α黻������

      CALL SVINIT(MM,NM,IM,JM,IT,T,P,R,JC,W)

      ISEED=1
      DO L=1,(MM+1)*(MM+1)
         CALL MLRAND(ISEED,RAN)
         S(L)=2*RAN-1
      END DO

      PRINT '(A,I5,A,I5,A,I5,A,I4)','MM=',MM,', IM=',IM,' JM=',JM,', NTR=',NTR

      CALL LVGCPU(ICPU)
      IF(ICPU.EQ.0) THEN
         PRINT '(A)','SSE=fort'
      ELSE IF(ICPU.EQ.10) THEN
         PRINT '(A)','SSE=avx'
      ELSE IF(ICPU.EQ.20) THEN
         PRINT '(A)','SSE=fma'
      ELSE IF(ICPU.EQ.30) THEN
         PRINT '(A)','SSE=avx512'
      ELSE IF(ICPU.EQ.100) THEN
         PRINT '(A)','SSE=sx'
      END IF

      MAXTD=1
!$    MAXTD=omp_get_max_threads()      
      PRINT '(A,I3)','number of threads =',MAXTD

      CALL MLTIME(TIM0)
      DO ITR=1,NTR
         CALL  SVTS2G(MM,NM,NN,IM,JM,S,G,IT,T,P,R,JC,WS,W,IPOW)
      END DO
      CALL MLTIME(TIM1)
      GFLOPS=RC*NTR/(TIM1-TIM0)/1D9
      PRINT '(A,ES9.2,A,F6.1,A)','S2G: ',(TIM1-TIM0)/NTR,' sec  (', &
     & GFLOPS,' GFlops)'

      CALL MLTIME(TIM0)
      DO ITR=1,NTR
         CALL  SVTG2S(MM,NM,NN,IM,JM,SD,G,IT,T,P,R,JC,WS,W,IPOW)
      END DO
      CALL MLTIME(TIM1)
      GFLOPS=RC*NTR/(TIM1-TIM0)/1D9
      PRINT '(A,ES9.2,A,F6.1,A)','G2S: ',(TIM1-TIM0)/NTR,' sec  (', &
     & GFLOPS,' GFlops)'

      SLMAX=0
      SLAMAX=0
      M=0
      DO N=0,MM
         CALL SVNM2L(MM,N,M,L)
         SL=ABS(SD(L)-S(L))
         IF(SL.GT.SLMAX) THEN
            SLMAX=SL
            LAS=L
         END IF
         SLAMAX=SLAMAX+SL**2
         !        print *,N,M,SL
      END DO

      DO M=1,MM
         DO N=M,MM
            CALL SVNM2L(MM,N,M,LR)
            CALL SVNM2L(MM,N,-M,LI)
            SL=(SD(LR)-S(LR))**2+(SD(LI)-S(LI))**2
            SL=SQRT(SL)
            IF(SL.GT.SLMAX) THEN
               SLMAX=SL
               LAS=LR
            END IF
            !          print *,N,M,SL
            SLAMAX=SLAMAX+SL**2
         END DO
      END DO
      CALL SVL2NM(MM,LAS,N,M)
      PRINT '(A,ES9.2,A,I5,A,I5,A)','maxerror =',SLMAX,' (n=',N,', m=',M,')'
      PRINT '(A,ES9.2)','rmserror =',SQRT(SLAMAX/((MM+1)*(MM+2)/2))

      DEALLOCATE(S)
      DEALLOCATE(SD)
      DEALLOCATE(T)
      DEALLOCATE(P)
      DEALLOCATE(R)
      DEALLOCATE(WS)
      DEALLOCATE(IT)
      DEALLOCATE(JC)
      CALL MLFREE(PG)
      CALL MLFREE(PW)
        
      END
