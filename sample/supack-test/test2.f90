!***********************************************************************
! ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
! Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
!
! This library is free software; you can redistribute it and/or
! modify it under the terms of the GNU Lesser General Public
! License as published by the Free Software Foundation; either
! version 2.1 of the License, or (at your option) any later version.
!
! This library is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! Lesser General Public License for more details.
! 
! You should have received a copy of the GNU Lesser General Public
! License along with this library; if not, write to the Free Software
! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
! 02110-1301 USA.
!***********************************************************************
!***********************************************************************
!     SUPACK 内での 逆変換と正変換のテスト
!      (与えられたスペクトルデータに対して, グリッド上の勾配を求め,
!       さらにその発散に対応するスペクトルデータを計算し, 最初に与えた      
!       スペクトルデータに直接ラプラシアンを作用させたものに一致するか      
!       どうかをチェックする)                                 2016/02/04
!***********************************************************************      
      USE ISO_C_BINDING
      IMPLICIT NONE
      INCLUDE 'mpif.h'
!      INTEGER,PARAMETER :: MM=170,JM=256,IM=512
      INTEGER,PARAMETER :: MM=682,JM=1024,IM=2048
      INTEGER,PARAMETER :: JV=4
      INTEGER,PARAMETER :: NM=MM+1
      INTEGER :: N,M,L,LR,LI,LAS,ISEED,ICPU,IERR,NP,IP
      REAL(8) :: SLMAX,RAN,SLAMAX,SL,EPS=1D-14*IM
      INTEGER,DIMENSION(:),ALLOCATABLE :: IT
      INTEGER,DIMENSION(:),ALLOCATABLE :: JC
      REAL(8),DIMENSION(:),ALLOCATABLE :: T
      REAL(8),DIMENSION(:),ALLOCATABLE :: WP
      REAL(8),DIMENSION(:,:),ALLOCATABLE :: P
      REAL(8),DIMENSION(:),ALLOCATABLE :: R
      REAL(8),DIMENSION(:),ALLOCATABLE :: S,SX,SY,SXR,SYD,SXD,SD,C,D
      REAL(8),DIMENSION(:,:),ALLOCATABLE :: WS
      REAL(8),DIMENSION(:),POINTER:: W
      TYPE(C_PTR) :: PW
      REAL(8),DIMENSION(:,:),ALLOCATABLE :: G

      REAL(8),DIMENSION(:),ALLOCATABLE :: SALL
      REAL(8),DIMENSION(:),ALLOCATABLE :: SDALL
      REAL(8),DIMENSION(:),ALLOCATABLE :: SBUF

      CALL MPI_INIT(IERR)
      CALL MPI_COMM_SIZE(MPI_COMM_WORLD,NP,IERR)
      CALL MPI_COMM_RANK(MPI_COMM_WORLD,IP,IERR)

      ALLOCATE(IT(IM/2))
      ALLOCATE(JC(5*(MM/NP+1)*(2*NM-MM/NP*NP+8)/8))
      ALLOCATE(T(IM))
      ALLOCATE(WP(JM/2))
      ALLOCATE(P(JM/2,5+2*(MM/NP+1)))
      ALLOCATE(R(5*(MM/NP+1)*(2*NM-MM/NP*NP)/4))
      ALLOCATE(C((MM/NP+1)*(2*(MM+1)-MM/NP*NP)))
      ALLOCATE(D((MM/NP+1)*(2*(MM+1)-MM/NP*NP)*2))
      ALLOCATE(S((MM/NP+1)*(2*(MM+1)-MM/NP*NP)))
      ALLOCATE(SD((MM/NP+1)*(2*(MM+1)-MM/NP*NP)))
      ALLOCATE(SX((MM/NP+1)*(2*(MM+1)-MM/NP*NP)))
      ALLOCATE(SXD((MM/NP+1)*(2*(MM+1)-MM/NP*NP)))
      ALLOCATE(SXR((MM/NP+1)*(2*(MM+1+1)-MM/NP*NP)))
      ALLOCATE(SY((MM/NP+1)*(2*(MM+1+1)-MM/NP*NP)))
      ALLOCATE(SYD((MM/NP+1)*(2*(MM+1)-MM/NP*NP)))
      ALLOCATE(WS((NM+1)*2,MM/NP+1))
      ALLOCATE(G(0:IM-1,((JM/JV-1)/NP+1)*JV))

      CALL MLALLC(PW,4*2*JV*((JM/JV-1)/NP+1)*((IM/2-1)/NP+1)*NP)
      CALL C_F_POINTER(PW, W, [4*2*JV*((JM/JV-1)/NP+1)*((IM/2-1)/NP+1)*NP])

      CALL SUINIT(MM,NM,IM,JM,IT,T,P,R,JC,WP)

      CALL SUINIC(MM,C)
      CALL SUINID(MM,D)
      
      IF(IP.EQ.0) THEN
         ALLOCATE(SALL((MM+1)*(MM+1)))
         ALLOCATE(SDALL((MM+1)*(MM+1)))
         ALLOCATE(SBUF((MM/NP+1)*(2*(MM+1)-MM/NP*NP)*NP))
         ISEED=1
         DO L=1,(MM+1)*(MM+1)
            CALL MLRAND(ISEED,RAN)
            SALL(L)=2*RAN-1
         END DO
         SALL(1)=0
      END IF

      CALL SUSS2S(MM,MM,SALL,S,SBUF)

      CALL SUCLAP(MM,S,SD,D,2)
      CALL SUCLAP(MM,SD,S,D,1)

      CALL SUCS2X(MM,SD,SX)
      CALL SUCRUP(MM,MM+1,SX,SXR)
      CALL SUCS2Y(MM,SD,SY,C)

      CALL SUTS2G(MM,NM,MM+1,IM,JM,JV,SXR,G,IT,T,P,R,JC,WS,W,1)
      CALL SUTG2S(MM,NM,MM+1,IM,JM,JV,SXR,G,IT,T,P,R,JC,WS,W,1)
      CALL SUTS2G(MM,NM,MM+1,IM,JM,JV,SY,G,IT,T,P,R,JC,WS,W,1)
      CALL SUTG2S(MM,NM,MM+1,IM,JM,JV,SY,G,IT,T,P,R,JC,WS,W,1)

      CALL SUCY2S(MM,SY,SYD,C)
      CALL SUCRDN(MM,MM+1,SXR,SX)
      CALL SUCS2X(MM,SX,SXD)

      SD=SXD+SYD

      CALL SUGS2S(MM,MM,S,SALL,SBUF)
      CALL SUGS2S(MM,MM,SD,SDALL,SBUF)

      IF(IP.EQ.0) THEN
         DEALLOCATE(SBUF)
         SLMAX=0
         SLAMAX=0
         M=0
         DO N=0,MM
            CALL SVNM2L(MM,N,M,L)
            SL=ABS(SDALL(L)-SALL(L))
            IF(SL.GT.SLMAX) THEN
               SLMAX=SL
               LAS=L
            END IF
            SLAMAX=SLAMAX+SL**2
         END DO

         DO M=1,MM
            DO N=M,MM
               CALL SVNM2L(MM,N,M,LR)
               CALL SVNM2L(MM,N,-M,LI)
               SL=(SDALL(LR)-SALL(LR))**2+(SDALL(LI)-SALL(LI))**2
               SL=SQRT(SL)
               IF(SL.GT.SLMAX) THEN
                  SLMAX=SL
                  LAS=LR
               END IF
               SLAMAX=SLAMAX+SL**2
            END DO
         END DO
         CALL SVL2NM(MM,LAS,N,M)
         PRINT '(A,ES9.2,A,I5,A,I5,A)','maxerror =',SLMAX,' (n=',N,', m=',M,')'
         PRINT '(A,ES9.2)','rmserror =',SQRT(SLAMAX/((MM+1)*(MM+2)/2))
         print *,'gradient and divergence check:'
         IF(SLMAX.LE.EPS) THEN
            print *,'** OK'
         ELSE
            print *,'** Fail'
         END IF
         DEALLOCATE(SALL)
         DEALLOCATE(SDALL)
      END IF

      DEALLOCATE(IT)
      DEALLOCATE(JC)
      DEALLOCATE(T)
      DEALLOCATE(WP)
      DEALLOCATE(P)
      DEALLOCATE(R)
      DEALLOCATE(C)
      DEALLOCATE(D)
      DEALLOCATE(S)
      DEALLOCATE(SD)
      DEALLOCATE(SX)
      DEALLOCATE(SXD)
      DEALLOCATE(SXR)
      DEALLOCATE(SY)
      DEALLOCATE(SYD)
      DEALLOCATE(WS)
      DEALLOCATE(G)
      CALL MLFREE(PW)

      CALL MPI_FINALIZE(IERR)

      END
