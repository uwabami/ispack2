Source: ispack2
Priority: optional
Maintainer: Youhei SASAKI <uwabami@gfd-dennou.org>
Build-Depends: debhelper (>= 11),
               mpi-default-dev,
               mpi-default-bin
Standards-Version: 4.2.1
Section: science
Homepage: https://www.gfd-dennou.org/arch/ispack/index.htm.en
Vcs-Browser: https://salsa.debian.org/uwabami-guest/ispack
Vcs-Git: https://salsa.debian.org/uwabami-guest/ispack.git

Package: libispack2-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libispack2-2 (= ${binary:Version}), ${misc:Depends}
Description: A FORTRAN77 library for scientific computing -- static library
 This library (ISPACK) is a package of basic tools (Spectral transform,
 etc.) as subroutines, which are required mainly for computations of
 simple fluid equations. Because each subroutine is designed to be used
 independently, this library is not an integrated environment in which
 you can run numerical models without sufficient knowledge. How to
 combine and use these subroutines is up to users.
 .
 This package provides static library.

Package: libispack2-2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: A FORTRAN77 library for scientific computing -- shared library
 This library (ISPACK) is a package of basic tools (Spectral transform,
 etc.) as subroutines, which are required mainly for computations of
 simple fluid equations. Because each subroutine is designed to be used
 independently, this library is not an integrated environment in which
 you can run numerical models without sufficient knowledge. How to
 combine and use these subroutines is up to users.
 .
 This package provides shared library.
