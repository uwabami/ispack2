########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl svqtb2_
.globl _svqtb2_	
svqtb2_:
_svqtb2_:	
      # rdi, rsi, rdx
	movl (%rdi),%edi # rdi に IM
	shlq $3,%rdi # rdi に IM*8
	lea (%rdi,%rdi),%r8
	lea (%r8,%rdi),%r9
	
#	lea (%rsi,%rdi),%rcx
	lea (%rdx,%rdi),%rcx	
L0:	

	vmovapd   (%rsi), %ymm0
	vmovapd 32(%rsi), %ymm1
	vmovapd 64(%rsi), %ymm2
	vmovapd 96(%rsi), %ymm3
	vunpcklpd %ymm1,%ymm0,%ymm14
	vunpckhpd %ymm1,%ymm0,%ymm0
	vunpcklpd %ymm3,%ymm2,%ymm15	
	vunpckhpd %ymm3,%ymm2,%ymm2
	vperm2f128 $32,%ymm2,%ymm0,%ymm1
	vperm2f128 $49,%ymm2,%ymm0,%ymm3
	vperm2f128 $32,%ymm15,%ymm14,%ymm0
	vperm2f128 $49,%ymm15,%ymm14,%ymm2

	vmovapd 128(%rsi), %ymm4
	vmovapd 160(%rsi), %ymm5
	vmovapd 192(%rsi), %ymm6
	vmovapd 224(%rsi), %ymm7
	vunpcklpd %ymm5,%ymm4,%ymm14
	vunpckhpd %ymm5,%ymm4,%ymm4
	vunpcklpd %ymm7,%ymm6,%ymm15	
	vunpckhpd %ymm7,%ymm6,%ymm6
	vperm2f128 $32,%ymm6,%ymm4,%ymm5
	vperm2f128 $49,%ymm6,%ymm4,%ymm7
	vperm2f128 $32,%ymm15,%ymm14,%ymm4
	vperm2f128 $49,%ymm15,%ymm14,%ymm6

	vmovapd %ymm0,(%rdx) # ymm0 空き
	vmovapd %ymm4,32(%rdx) # ymm4 空き

	vmovapd 256(%rsi), %ymm8
	vmovapd 288(%rsi), %ymm9
	vmovapd 320(%rsi), %ymm10
	vmovapd 352(%rsi), %ymm11
	vunpcklpd %ymm9,%ymm8,%ymm14
	vunpckhpd %ymm9,%ymm8,%ymm8
	vunpcklpd %ymm11,%ymm10,%ymm15	
	vunpckhpd %ymm11,%ymm10,%ymm10
	vperm2f128 $32,%ymm10,%ymm8,%ymm9
	vperm2f128 $49,%ymm10,%ymm8,%ymm11
	vperm2f128 $32,%ymm15,%ymm14,%ymm8
	vperm2f128 $49,%ymm15,%ymm14,%ymm10

	vmovapd 384(%rsi), %ymm12
	vmovapd 416(%rsi), %ymm13
	vmovapd 448(%rsi), %ymm14
	vmovapd 480(%rsi), %ymm15
	vunpcklpd %ymm13,%ymm12,%ymm0
	vunpckhpd %ymm13,%ymm12,%ymm12
	vunpcklpd %ymm15,%ymm14,%ymm4
	vunpckhpd %ymm15,%ymm14,%ymm14
	vperm2f128 $32,%ymm14,%ymm12,%ymm13
	vperm2f128 $49,%ymm14,%ymm12,%ymm15
	vperm2f128 $32,%ymm4,%ymm0,%ymm12
	vperm2f128 $49,%ymm4,%ymm0,%ymm14

	vmovapd %ymm8,64(%rdx)
	vmovapd %ymm12,96(%rdx)	
	
	vmovapd %ymm1,(%rdx,%rdi)
	vmovapd %ymm5,32(%rdx,%rdi)
	vmovapd %ymm9,64(%rdx,%rdi)
	vmovapd %ymm13,96(%rdx,%rdi)			

	vmovapd %ymm2,(%rdx,%r8)
	vmovapd %ymm6,32(%rdx,%r8)
	vmovapd %ymm10,64(%rdx,%r8)
	vmovapd %ymm14,96(%rdx,%r8)		
	
	vmovapd %ymm3,(%rdx,%r9)
	vmovapd %ymm7,32(%rdx,%r9)	
	vmovapd %ymm11,64(%rdx,%r9)
	vmovapd %ymm15,96(%rdx,%r9)	
	
#	addq $128,%rsi
	addq $512,%rsi
	addq $128,%rdx	
#	cmpq %rsi,%rcx
	cmpq %rdx,%rcx	
	jne L0
       
	ret
