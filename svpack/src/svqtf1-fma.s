########################################################################
# ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
# Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
# 
# You should have received a copy of the GNU Lesser General Public
# License along with this library; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301 USA.
########################################################################
.text
.globl svqtf1_
.globl _svqtf1_	
svqtf1_:
_svqtf1_:	
      # rdi, rsi, rdx, rcx, r8, r9
	movl (%rdi),%edi # rdi に JM/JV
	shlq $6,%rdi # rdi に (JM/JV)*64
	movl (%rsi),%esi # rsi に MM
	shlq $6,%rsi # rsi に MM*64
	movl (%rdx),%edx # rdx に IM
	shlq $5,%rdx # rdx に IM/2*64
	
	# rcx が W, r8 が G

	addq %rcx,%rsi
	addq $64,%rsi	
	addq %rcx,%rdx		
L0:
	vmovapd   (%rcx), %ymm0
	vmovapd 32(%rcx), %ymm1
	vmovapd %ymm0,  (%r8)
	vmovapd %ymm1,32(%r8)	

	addq $64,%rcx
	addq %rdi,%r8
	cmpq %rcx,%rsi
	jne L0

	ret
