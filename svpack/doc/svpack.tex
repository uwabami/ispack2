%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%
% last modified 2017/08/30
%   correct typo erros.
%
\documentclass[a4paper]{scrartcl}


\title{Manual of SVPACK}
\author{}
\date{}

\begin{document}

\maketitle

\section{Outline}

This is a package of subroutines to compute the spectral (spherical
harmonics) transform. It consists of subroutines that compute
transform from the coefficients of spherical harmonics to the grid 
values and the reverse transform, and ancillary subroutines.
This package is an upper-level package based on FVPACK and LVPACK.

Let $m$ and $n$ be the zonal and the total wavenumbers of
the spherical harmonics, respectively, and 
let the truncation wavenumbers for them be $M$ and $N$,
respectively. Here, $N\ge M$ is assumed (when $N=M$, it corresponds
to the triangular truncation).
Using this notation, the spectral transform
under these truncation wavenumbers is written as follows.
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^N_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
Introducing the associated Legendre function backward transform:
\begin{equation}
G^m(\varphi)\equiv\sum^N_{n=|m|}s^m_nP^m_n(\sin\varphi),
\end{equation}
it can be written as,
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}G^m(\varphi)e^{im\lambda},
\end{equation}
which is a synthesis of 
the associated Legendre function backward transform
and the backward Fourier transform.
Here, $\lambda$ is the longitude, and $\varphi$ is the latitude.

Here, $\varphi$ is the latitude and
$P^m_n(\mu)$ is the associated Legendre function normalized to 2,
which is defined as:
\begin{equation}
P^m_n(\mu)\equiv\sqrt{(2n+1)\frac{(n-|m|)!}{(n+|m|)!}}
\frac1{2^nn!}(1-\mu^2)^{|m|/2}
\frac{d^{n+|m|}}{d\mu^{n+|m|}}(\mu^2-1)^n,
\end{equation}
\begin{equation}
\int^1_{-1}\{P^m_n(\mu)\}^2d\mu=2.
\end{equation}

For $g(\lambda,\varphi)$ to be a real function,
$s^m_n$ and $G^m(\varphi)$ must satisfy the following:
\begin{equation}
G^{-m}(\varphi)=\{G^m(\varphi)\}^{*},
\end{equation}
\begin{equation}
s^{-m}_n=\{s^m_n\}^{*}.
\end{equation}
Here, $\{ \}^{*}$ denotes the complex conjugate.
Hence, $G^m(\sin\varphi)$ and $s^m_n$ should be
treated for only $m\ge 0$.
Accordingly, $G^0(\sin\varphi)$ and $s^0_n$ take real values.

The forward spectral transform is written as follows.
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)P^m_n(\sin\varphi)e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\end{equation}
Similarly as the backward transform, introducing the forward
Fourier transform as,
\begin{equation}
G^m(\varphi)\equiv\frac1{2\pi}\int^{2\pi}_0
g(\lambda,\varphi)e^{-im\lambda}d\lambda,
\end{equation}
it can be written by a synthesis of 
the forward Fourier transform and the forward 
associated Legendre function transform as,
\begin{equation}
s^m_n=\frac12\int^{\pi/2}_{-\pi/2}G^m(\varphi)P^m_n(\sin\varphi)\cos\varphi.
d\varphi
\end{equation}

In numerical calculation, the integration shown above is approximated
by discretization. The forward Fourier transform is approximated
by the discrete forward Fourier transform using the values at
points at an equal interval in the longitude and the forward 
associated Legendre function 
transform is approximated as follows by using the Gauss-Legendre
integration formula as,
\begin{equation}
s^m_n=\frac12\sum^J_{j=1}w_jG^m(\varphi_j)P^m_n(\sin\varphi_j).
\end{equation}
Here, $\varphi_j$s are called Gaussian latitude, which is defined as
the $J$ zero points (sorted ascending order) of the Legendre
polynomial $P_J(\sin\varphi)$ (this corresponds to 
the associated Legendre function of $m=0$ without the normalization
factor (the factor in $\sqrt{\quad}$), and $w_j$ is 
the Gaussian weight corresponding to each Gaussian latitude, 
which is defined as,
\begin{equation}
w_j\equiv\frac{2(1-\mu_j^2)}{\{JP_{J-1}(\mu_j)\}^2}.
\end{equation}
Here, $\mu_j\equiv\sin\varphi_j$.
In a certain condition, this integration formula gives the 
perfect approximation or the same value as the original integration.

This subroutine package contains
subroutines that do backward transform of spectral data ($s^m_n$) 
to grid data ($g(\lambda_i,\varphi_j)$),
subroutines that do forward transform of
grid data ($g(\lambda_i,\varphi_j)$),
to spectral data ($s^m_n$), and ancillary subroutines.

Here, the longitude of each grid point, $\lambda_i$, 
is $I$-equally spaced longitudes around a latitude circle,
which is written as,
$\lambda_i=2\pi i/I\quad (i=0,1,\ldots,I-1)$
(note that $i$ is not the imaginary unit here),
and $\varphi_j$s are $J$ Gaussian latitudes defined above.

In the following explanations for subroutines, the notations below
are used.
\begin{center}
\begin{tabular}{ll}
\texttt{MM}:& $M$(the truncation number for $m$)\\
\texttt{NN}:& $N$(the truncation number for $n$)\\
\texttt{NM}:& maximum value of $N$ to be used\\
\texttt{MT}:& the truncation wavenumber for the triangular truncation\\
\texttt{JM}:& $J$(the number of Gaussian latitudes)\\
\texttt{IM}:& $I$(the number of grids in longitude)\\
\texttt{N}:& $n$(the total wavenumber)\\
\texttt{M}:& $m$(the zonal wavenumber)\\
\texttt{J}:& $j$(the index for each Gaussian latitude)\\
\texttt{I}:& $i$(the index for each longitude)
\end{tabular}
\end{center}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{List of subroutines}

\vspace{1em}
\begin{tabular}{ll}
\texttt{SVINIT} & Initialization\\
\texttt{SVNM2L} & Compute the position where spectral data are stored\\
\texttt{SVL2NM} & Inverse computation of \texttt{SVNM2L}\\
\texttt{SVTS2G} & Transform from spectral data to grid data\\
\texttt{SVTG2S} & Transform from grid data to spectral data\\
\texttt{SVINIC} & Initialize an array used in \texttt{SVCS2Y}, \texttt{SVCY2S}, 
\textit{etc}\\
\texttt{SVCS2Y/SVCSZY/SVCSWY} & Transform of spectral data 
corresponding to latitudinal derivative\\
\texttt{SVCY2S/SVCYZS/SVCYWS} & Transform of spectral data \\
& corresponding to forward spectral transform with latitudinal derivative\\
\texttt{SVCS2X} & Transform of spectral data 
corresponding to longitudinal derivative\\
\texttt{SVINID} & Initialize arrays used in \texttt{SVCLAP}\\
\texttt{SVCLAP} &  Transform of spectral data \\
& corresponding to operating Laplacian or its inverse\\
\texttt{SVCRUP} & Repacking spectral data from \\
& the triangular truncation ($N=M$) to another truncation ($N>M$)\\
\texttt{SVCRDN} & Inverse operation of \texttt{SVCRUP}
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Usage of each subroutine}

\subsection{SVINIT}

\begin{enumerate}

\item Purpose

Initialization routine for \texttt{SVPACK}.
It initializes the arrays, \texttt{IT, T, P, R, JC},
which are used other subroutines in \texttt{SVPACK}

\item Definition

\item Synopsis 
    
\texttt{SVINIT(MM,NM,IM,JM,IT,T,P,R,JC,WP)}
  
\item Parameters 
    
\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$(the truncation number for $m$)\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$(the truncation 
  number for $n$) \\
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{IT}  & \texttt{(I(IM/2))}    & Output. An array which is used in other routines in SVPACK.\\
\texttt{T}   & \texttt{(D(IM))} & Output. An array which is used in other routines in SVPACK.\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & Output. An array which is used in other routines in SVPACK.\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & Output. An array which is used in other routines in SVPACK.\\
\texttt{JC}  & \multicolumn{2}{l}{\texttt{(I(MM*(2*NM-MM-1)/8+MM))}}\\
 &   & Output. An array which is used in other routines in SVPACK.\\
\texttt{WP}  & \texttt{(D(JM/2*MM))} 
      & Working area.
\end{tabular}

\item Remark

(a) \texttt{MM} must be a natural number.
 When the triangular truncation is assumed and
 the latitudinal derivative is needed (see SVCS2Y),
 you should set as \texttt{MM=MT} and \texttt{NM=MT+1}.
 Here, \texttt{MT} is the truncation wavenumber for 
  the triangular truncation
     
\texttt{JM} must be an even number ($\ge 2$).
Maximum efficiency is achieved by setting \texttt{JM} to be 
a multiple of 8 when you do make ISPACK with setting either
SSE=avx or SSE=fma.
When setting SSE=avx512, maximum efficiency is achieved
by setting 
\texttt{JM} to be a multiple of 16.

\texttt{IM} must be a power of two which satisfies
\texttt{IM > 2*MM}.

\texttt{NM} must satisfy 
\texttt{NM} $\ge$ \texttt{MM}.

(b) While using \texttt{SVPACK}, the arrays \texttt{IT, T, P, R, JC}
must not be changed.

(c) When \texttt{P} is declared as \texttt{P(JM/2,5+2*MM)},
   \texttt{P(J,1)}:  $\sin(\varphi_{J/2+j})$,
   \texttt{P(J,2)}:  $\frac12 w_{J/2+j}$, 
   \texttt{P(J,3)}:  $\cos(\varphi_{J/2+j})$,
   \texttt{P(J,4)}:  $1/\cos(\varphi_{J/2+j})$,
are contained.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVNM2L}

\begin{enumerate}

\item Purpose 

Compute the position where spectral data are stored
from the total wavenumber and the zonal wavenumber.

\item Definition

As described in Outline, 
spectral data ($s^m_n$) have $(2N+1-M)M+N+1$ independent
components. In SVPACK, they are stored in an array the
size of which is $(2N+1-M)M+N+1$.

The order of storing is as follows, depending the truncation wavenumber:
\begin{eqnarray*}
&& s^0_0, s^0_1, \ldots, s^0_N,\\
&& \mbox{Re}(s^1_1), \mbox{Im}(s^1_1),
\mbox{Re}(s^1_2), \mbox{Im}(s^1_2), \ldots, 
\mbox{Re}(s^1_N), \mbox{Im}(s^1_N),\\
&&\ldots,\\
&& \mbox{Re}(s^M_M), \mbox{Im}(s^M_M), \ldots,
\ldots \mbox{Re}(s^M_N), \mbox{Im}(s^M_N).\\
\end{eqnarray*}
This subroutine computes 
the position in the array ($s^m_n$)
where spectral data are stored
from the total wavenumber($n$) and the zonal wavenumber($m$).

\item Synopsis 
    
\texttt{SVNM2L(NN,N,M,L)}
  
\item Parameters 

\begin{tabular}{lll}
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{N} & \texttt{(I)} & Input. $n$(the total wavenumber)\\
\texttt{M} & \texttt{(I)} & Input. $m$(the zonal wavenumber. See Remark)\\
\texttt{L} & \texttt{(I)} & Output. the position where the spectral
data is stored.
\end{tabular}

\item Remark

If \texttt{M} $>$ 0, it returns the position where
$\mbox{Re}(s^m_n)$ is stored with setting 
$m=$ \texttt{M} and $n=$ \texttt{N}.
If \texttt{M} $<$ 0, it returns the position where
$\mbox{Im}(s^m_n)$ is stored with setting 
$m=$ \texttt{-M} and $n=$ \texttt{N}.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVL2NM}

\begin{enumerate}

\item Purpose 

Inverse computation of \texttt{SVNM2L}.
That is, 
it computes 
the total wavenumber and the zonal wavenumber from
the position where spectral data are stored.

\item Definition

See \texttt{SVNM2L}.

\item Synopsis 
    
\texttt{SVL2NM(NN,L,N,M)}
  
\item Parameters 

\begin{tabular}{lll}
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{L} & \texttt{(I)} & Input. The position where the spectral
data are stored.\\
\texttt{N} & \texttt{(I)} & Output. $n$(the total wavenumber)\\
\texttt{M} & \texttt{(I)} & Output. $m$(the zonal wavenumber)\\
\end{tabular}

\item Remark

 The meaning of the sign of \texttt{M} is the same as in \texttt{SVNM2L}.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVTS2G}

\begin{enumerate}

\item Purpose 

Transform from spectral data to grid data.

\item Definition

Transform spectral data ($s^m_n$) 
to grid data ($g(\lambda_i,\varphi_j)$)
by the backward spectral transform (see Outline).

\item Synopsis 

\texttt{SVTS2G(MM,NM,NN,IM,JM,S,G,IT,T,P,R,JC,WS,W,IPOW)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & Input. $M$\\
\texttt{NM} & \texttt{(I)} & Input. Maximum value of $N$ to be used\\
\texttt{NN} & \texttt{(I)} & Input. $N$
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM} must hold)\\
\texttt{IM} & \texttt{(I)} & Input. The number of grids in longitude\\
\texttt{JM} & \texttt{(I)} & Input. The number of Gaussian latitudes.\\
\texttt{S} & \texttt{(D((2*NN+1-MM)*MM+NN+1))} & Input. Array that
contains $s^m_n$\\
\texttt{G} & \texttt{(D(JM*IM))} & Output. Array to contain $g(\lambda_i,\varphi_j)$\\
\texttt{IT}  & \texttt{(I(IM/2))}    & Input. Array initialized by SVINIT\\
\texttt{T}   & \texttt{(D(IM))} & Input. Array initialized by SVINIT\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & Input. Array initialized by SVINIT\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & Input. Array initialized by SVINIT\\
\texttt{JC}  & \texttt{(I(MM*(2*NM-MM-1)/8+MM))}  & Input. Array
initialized by SVINIT\\
\texttt{WS} & \texttt{(D((NN+1)*2*(MM+1)))} & Working area\\
\texttt{W} & \texttt{(D(JM*IM*2))} & Working area\\
\texttt{IPOW} & \texttt{(I)} & Input. The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) If \texttt{G} is declared as \texttt{G(0:IM-1,JM)},
$g(\lambda_i,\varphi_j)$ is to be contained in \texttt{G(I,J)}.

(b) The array \texttt{G} and \texttt{W} must be aligned with 32byte boundary
if you did make ISPACK with setting either SSE=avx or SSE=fma
on Intel x86 CPUs and setting \texttt{JM} to be a multiple of 8.
The array \texttt{G} must be aligned with 64byte boundary
if you did make ISPACK with setting SSE=avx512
on Intel x86 CPUs and setting \texttt{JM} to be a multiple of 16.

(c) If you set \texttt{IPOW}$=l$, 
$(\cos\varphi_j)^{-l}g(\lambda_i,\varphi_j)$ is returned 
in the output array \texttt{G} instead of $g(\lambda_i,\varphi_j)$.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVTG2S}

\begin{enumerate}

\item Purpose 

Transform from grid data to spectral data.

\item Definition

Transform wave data ($g(\lambda_i,\varphi_j)$)
to spectral data ($s^m_n$)
by the forward spectral transform (see Outline).

\item Synopsis 

\texttt{SVTG2S(MM,NM,NN,IM,JM,S,G,IT,T,P,R,JC,WS,W,IPOW)}

  
\item Parameters

(Since most of parameters are the same as in SVTS2G,
the following explanation is only for parameters
different from those in SVTS2G,)

\begin{tabular}{lll}
\texttt{S} & \texttt{(D((2*NN+1-MM)*MM+NN+1))} & Output Array to contain $s^m_n$\\
\texttt{G} & \texttt{(D(JM*IM))} & Input. Array that 
contains $g(\lambda_i,\varphi_j)$\\
\texttt{IPOW} & \texttt{(I)} & Input. 
The degree of $1/\cos\varphi$ multiplied 
simultaneously  \\
& & with the transform. An integer between 0 and 2.
\end{tabular}

\item Remark

(a) If \texttt{G} is declared as \texttt{G(0:IM-1,JM)},
\texttt{G(I,J)} should contain $g(\lambda_i,\varphi_j)$.

(b) If you set \texttt{IPOW}$=l$, 
$(\cos\varphi_j)^{-l}g(\lambda_i,\varphi_j)$ is used as the input
instead of $g(\lambda_i,\varphi_j)$.
   
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVINIC}

\begin{enumerate}

\item Purpose 

Initialize an array used in \texttt{SVCS2Y}, \texttt{SVCY2S}, 
\textit{etc}

\item Definition

See SVCS2Y and SVCY2S.

\item Synopsis 

\texttt{SVINIC(MT,C)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. 
The truncation wavenumber for the triangular truncation.\\
\texttt{C} & \texttt{(D((MT+1)*(MT+1)))} & Output. 
Array used in \texttt{SVCS2Y}, \texttt{SVCY2S}, 
\textit{etc}.
\end{tabular}

\item Remark

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCS2Y/SVCSZY/SVCSWY}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to latitudinal derivative

\item Definition

If the backward spectral transform, where the triangular
truncation of $N=M$ is assumed, is defined as,
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^M_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda},
\end{equation}
operating $\cos\varphi\frac{\partial}{\partial\varphi}$ to
$g$ yields
\begin{equation}
\cos\varphi\frac{\partial}{\partial\varphi}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^{M+1}_{n=|m|}
(s_y)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
That is, it can be represented by a backward spectral transform
with setting the truncation wavenumber of $n$ as $N=M+1$.
This subroutine computes $(s_y)^m_n$ from $s^m_n$.

\item Synopsis 

\texttt{SVCS2Y(MT,S,SY,C)}\\
\texttt{SVCSZY(MT,S,SY,C)}\\
\texttt{SVCSWY(MT,M,S,SY,C)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{M} & \texttt{(I)} & Input. $m$(the zonal wavenumber)\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & Input. Array that
contains $s^m_n$\\
\texttt{SY} & \texttt{(D((MT+4)*MT+2))} & Output. Array to contain 
$(s_y)^m_n$\\
\texttt{C} & \texttt{(D((MT+1)*(MT+1)))} & Input. 
Array initialized by SVINIC\\
\end{tabular}

\item Remark

(a) If you call\texttt{SVCS2Y(MT,S,SY,C)} and\\
    \texttt{SVTS2G(MT,NM,MT+1,IM,JM,SY,G,IT,T,P,Q,R,WS,WG,W,1)}
    continuously, the latitudinal derivative is returned in \texttt{G}.

(b) \texttt{SVCSZY} treats the zonal component ($m=0$).
In this case, if \texttt{S} and \texttt{SY} are declared as
    \texttt{S(0:MT)} and \texttt{SY(0:MT+1)}, respectively,
  \texttt{S(N)} and \texttt{SY(N)} correspond to 
    $s^0_n$ and $(s_y)^0_n$, respectively.

(c) \texttt{SVCSWY}
treats a wave component ($m>0$).
In this case, if \texttt{S} and \texttt{SY} are declared as
    \texttt{S(2,M:MT)} and \texttt{SY(2,M:MT+1)}, respectively,
\texttt{S(1,N)}, \texttt{S(2,N)}, \texttt{SY(1,N)}, and
\texttt{SY(2,N)},
correspond to 
$\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$, $\mbox{Re}((s_y)^m_n)$, and
    $\mbox{Im}((s_y)^m_n)$ 
    $\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$, $\mbox{Re}((s_y)^m_n)$, $\mbox{Im}((s_y)^m_n)$, respectively.
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCY2S/SVCYZS/SVCYWS}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to the forward spectral transform with latitudinal derivative

\item Definition

If you want to compute the following form of modified forward
spectral transform
(it appers in computing the divergence of a vector field, for example):
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)
\left(-\cos\varphi\frac{\partial}{\partial\varphi}P^m_n(\sin\varphi)\right)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda,
\quad 
\end{equation}
(where, $m$ and $n$ are within the triangular truncation of $M$),
$s^m_n$ can be obtained from the result of 
the following normal forward spectral
transform of $g$:
\begin{equation}
(s_y)^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)
P^m_n(\sin\varphi)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda
\quad (n\le M+1=N).
\end{equation}
This subroutine computes $s^m_n$ from $(s_y)^m_n$.

\item Synopsis 

\texttt{SVCY2S(MT,SY,S,C)}\\
\texttt{SVCYZS(MT,SY,S,C)}\\
\texttt{SVCYWS(MT,M,SY,S,C)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{M} & \texttt{(I)} & Input. $m$(the zonal wavenumber)\\
\texttt{SY} & \texttt{(D((MT+4)*MT+2))} & Input. Array that
contains $(s_y)^m_n$\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & Output. Array to contain $s^m_n$\\
\texttt{C} & \texttt{(D((MT+1)*(MT+1)))} & Input. Array initialized by SVINIC\\
\end{tabular}

\item Remark

(a)If you call
  \texttt{SVTG2S(MT,NM,MT+1,IM,JM,SY,G,IT,T,P,Q,R,WS,WG,W,1)} and \\
      \texttt{SVCY2S(MT,SY,S,C)} continuously, 
    the following computation that corresponds to
    the forward transform of the divergence of a vector field,
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
\frac{\partial}{\cos\varphi\partial\varphi}
\left(\cos\varphi g(\lambda,\varphi)\right)
P^m_n(\sin\varphi)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda
\end{equation}
is done and $s^m_n$ is returned in \texttt{S}.
Note that integration by parts is applied and \texttt{IPOW=1} here.

(b) \texttt{SVCYZS} treats the zonal component ($m=0$).
In this case, if \texttt{S} and \texttt{SY} are declared as
    \texttt{S(0:MT)} and \texttt{SY(0:MT+1)}, respectively,
  \texttt{S(N)} and \texttt{SY(N)} correspond to 
    $s^0_n$ and $(s_y)^0_n$, respectively.

(c) \texttt{SVCYWS} treats a wave component ($m>0$).
In this case, if \texttt{S} and \texttt{SY} are declared as
    \texttt{S(2,M:MT)} and \texttt{SY(2,M:MT+1)}, respectively,
\texttt{S(1,N)}, \texttt{S(2,N)}, \texttt{SY(1,N)}, and
\texttt{SY(2,N)},
correspond to 
$\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$, $\mbox{Re}((s_y)^m_n)$, and
    $\mbox{Im}((s_y)^m_n)$ 
    $\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$, $\mbox{Re}((s_y)^m_n)$, $\mbox{Im}((s_y)^m_n)$, respectively.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCS2X}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to longitudinal derivative.

\item Definition

If the backward spectral transform, where the triangular
truncation of $N=M$ is assumed, is defined as,
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^M_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda},
\end{equation}
operating $\frac{\partial}{\partial\lambda}$ to $g$
yields
\begin{equation}
\frac{\partial}{\partial\lambda}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^{M}_{n=|m|}
(s_x)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
Here, $(s_x)^m_n=ims^m_n$.
This subroutine computes $(s_x)^m_n=ims^m_n$ from $s^m_n$.

\item Synopsis 

\texttt{SVCS2X(MT,S,SX)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & Input. Array that
contains $s^m_n$\\
\texttt{SX} & \texttt{(D((MT+1)*(MT+1)))} & Output. Array to contain 
$(s_x)^m_n$\\
\end{tabular}

\item Remark

(a) The order of storing $(s_x)^m_n$ is the same as that of $s^m_n$.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVINID}

\begin{enumerate}

\item Purpose 

Initialize array \texttt{D} used in SVCLAP. 

\item Definition

see SVCLAP.

\item Synopsis 

\texttt{SVINID(MT,D)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber for the triangular truncation.\\
\texttt{D} & \texttt{(D((MT+1)*(MT+1)*2))} & Output. Array used in SVCLAP
\end{tabular}

\item Remark

    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCLAP}

\begin{enumerate}

\item Purpose 

Transform of spectral data 
corresponding to Laplacian or its inverse.

\item Definition

For the spherical harmonics expansion (here, the triangular
truncation of $N=M$ is assumed):
\begin{equation}
g(\lambda,\varphi)=\sum^M_{n=0}\sum^n_{m=-n}
s^m_nP^m_n(\sin\varphi)e^{im\lambda},
\end{equation}
operating the horizontal Laplacian:
\begin{equation}
\nabla^2\equiv
\frac{\partial^2}{\cos^2\varphi\partial\lambda^2}
+\frac{\partial}{\cos\varphi\partial\varphi}\left(\cos\varphi\frac{\partial}{\partial\varphi}\right)
\end{equation}
yields the following 
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}-n(n+1)a^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
by the nature of spherical harmonics.
Hence, 
introducing
\begin{equation}
(s_l)^m_n\equiv -n(n+1)s^m_n,
\end{equation}
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}(s_l)^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
holds. 
Inversely, when
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}s^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
holds, introducing
\begin{equation}
(s_l)^m_n\equiv -\frac1{n(n+1)}s^m_n
\end{equation}
yields
\begin{equation}
g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}(s_l)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}

This subroutine computes $(s_l)^m_n = -n(n+1)s^m_n$
from $s^m_n$, or inversely computes $(s_l)^m_n = -s^m_n/(n(n+1))$
from $s^m_n$.

\item Synopsis 
    
\texttt{SVCLAP(MT,S,SL,D,IFLAG)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber \\
& & for the triangular truncation.\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & Input. 
Array that
contains $s^m_n$\\
\texttt{SL} & \texttt{(D((MT+1)*(MT+1)))} & Output. 
Array to contain $(s_l)^m_n$\\
\texttt{D} & \texttt{(D((MT+1)*(MT+1)*2))} & Input.
Array initialized by SVINID\\
\texttt{IFLAG} & \texttt{(I)} & Input. If \texttt{IFLAG=1}, it
operates Laplacian, \\
& & and if \texttt{IFLAG=2}, it operates the 
inverse of Laplacian.
\end{tabular}

\item Remark

(a) If \texttt{IFLAG=2}, $s^0_0$ is returned in $(s_l)^0_0$ (where $n=0$).

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCRUP}

\begin{enumerate}

\item Purpose

Repacking spectral data from 
the triangular truncation ($N=M$) to another truncation ($N>M$)

\item Definition

It is often the case that one wants the addition of the results
of \texttt{SVCS2Y} and \texttt{SVCS2X}. However, they are different
types of spectral data because their truncation wavenumbers $N$ for
$n$ are different and so they can not be added with each other
directly. This subroutine 
repacks spectral data from ($s^m_n$) of
the triangular truncation ($N=M$) to ($(s_r)^m_n$)
of another truncation ($N>M$).
Note that zeros are returned in $(s_r)^m_n$ where $n>M$.

\item Synopsis 
    
\texttt{SVCRUP(MT,NN,S,SR)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. 
The truncation wavenumber \\
& & for the triangular truncation.\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & Input. Array that contains 
$s^m_n$\\
\texttt{SR} & \texttt{(D((2*NN+1-MT)*MT+NN+1))} & Output. 
Array to contain $(s_r)^m_n$
\end{tabular}

\item Remark

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCRDN}

\begin{enumerate}

\item Purpose

Inverse operation of \texttt{SVCRUP}. That is, 
repacks spectral data from 
a truncation ($N>M$) to
the triangular truncation ($N=M$).

\item Definition

This subroutine 
repacks spectral data from 
($(s_r)^m_n$) of a truncation ($N>M$)
to ($s^m_n$) of
the triangular truncation ($N=M$).
Note that the data in $(s_r)^m_n$ where $n>M$ are
discarded.

\item Synopsis 
    
\texttt{SVCRDN(MT,NN,SR,S)}
  
\item Parameters

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & Input. The truncation wavenumber \\
& & for the triangular truncation.\\
\texttt{NN} & \texttt{(I)} & Input. $N$(the truncation number for $n$)\\
\texttt{SR} & \texttt{(D((2*NN+1-MT)*MT+NN+1))} & Input. 
Atray that contains $(s_r)^m_n$\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & Output. 
Array to contain $s^m_n$
\end{tabular}

\item Remark

\end{enumerate}


\end{document}
