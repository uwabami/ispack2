%***********************************************************************
% ISPACK FORTRAN SUBROUTINE LIBRARY FOR SCIENTIFIC COMPUTING
% Copyright (C) 1998--2017 Keiichi Ishioka <ishioka@gfd-dennou.org>
%
% This library is free software; you can redistribute it and/or
% modify it under the terms of the GNU Lesser General Public
% License as published by the Free Software Foundation; either
% version 2.1 of the License, or (at your option) any later version.
%
% This library is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% Lesser General Public License for more details.
% 
% You should have received a copy of the GNU Lesser General Public
% License along with this library; if not, write to the Free Software
% Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
% 02110-1301 USA.
%***********************************************************************
%
% 最終更新 2017/08/30
%  誤植の修正(主に括弧の整合性)
%
\documentclass[a4j]{jsarticle}


\title{SVPACK使用の手引}
\author{}
\date{}

\begin{document}

\maketitle

\section{概要}

これは, スペクトル(球面調和関数)変換を行なうサブルーチンパッケージであ
り, 球面調和関数展開の係数から格子点値, およびその逆の変換を行なうサブ
ルーチン, また, その他の補助ルーチンなどからなっている. 
また, このパッケージは FVPACK と LVPACK の上位パッケージであり, 
これらのパッケージを内部で引用している.

球面調和関数の東西波数を$m$, 全波数を$n$とし, 切断波数をそれぞれ
$M$, $N$とする(ただし, $N\ge M$ とする. $N=M$としたときが三角切断
である). このとき, この切断波数におけるスペクトル逆変換は, 以下の
ように表せる.
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^N_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
または, ルジャンドル陪関数逆変換:
\begin{equation}
G^m(\varphi)\equiv\sum^N_{n=|m|}s^m_nP^m_n(\sin\varphi)
\end{equation}
を導入すると, 
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}G^m(\varphi)e^{im\lambda}
\end{equation}
と, ルジャンドル逆変換とフーリエ逆変換の積として表される.
ここに, $\lambda$: 経度, $\varphi$: 緯度である.

また, $P^m_n(\mu)$は2に正規化されたルジャンドル陪関数で, 以下のように
定義される:
\begin{equation}
P^m_n(\mu)\equiv\sqrt{(2n+1)\frac{(n-|m|)!}{(n+|m|)!}}
\frac1{2^nn!}(1-\mu^2)^{|m|/2}
\frac{d^{n+|m|}}{d\mu^{n+|m|}}(\mu^2-1)^n,
\end{equation}
\begin{equation}
\int^1_{-1}\{P^m_n(\mu)\}^2d\mu=2.
\end{equation}

$g(\lambda,\varphi)$が実数であるとすると, $s^m_n$および
$G^m(\varphi)$は以下の関係を満たしている必要がある.
\begin{equation}
G^{-m}(\varphi)=\{G^m(\varphi)\}^{*}
\end{equation}
\begin{equation}
s^{-m}_n=\{s^m_n\}^{*}
\end{equation}
ここに, $\{ \}^{*}$は複素共役を表す.
従って, $G^m(\sin\varphi)$および$s^m_n$は$m\ge 0$の範囲だけを求めれば
良い. さらに, 上の制約から, $G^0(\sin\varphi)$および$s^0_n$は実数である.

また, スペクトル正変換は以下のように表せる.
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)P^m_n(\sin\varphi)e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\end{equation}
逆変換の場合と同様に, フーリエ正変換を,
\begin{equation}
G^m(\varphi)\equiv\frac1{2\pi}\int^{2\pi}_0
g(\lambda,\varphi)e^{-im\lambda}d\lambda
\end{equation}
と導入すると, 
\begin{equation}
s^m_n=\frac12\int^{\pi/2}_{-\pi/2}G^m(\varphi)P^m_n(\sin\varphi)\cos\varphi
d\varphi
\end{equation}
と, フーリエ正変換とルジャンドル正変換の合成として表される.

数値計算においては, 上記の積分はそれぞれ離散近似される. フーリエ正変
換の部分は経度方向の等間隔格子点上での値を用いた離散フーリエ正変換
によって近似し,
ルジャンドル正変換の部分は, ガウス-ルジャンドル積分公式により,
\begin{equation}
s^m_n=\frac12\sum^J_{j=1}w_jG^m(\varphi_j)P^m_n(\sin\varphi_j)
\end{equation}
として近似する. ここに, $\varphi_j$はガウス緯度と呼ば
れる分点で, ルジャンドル多項式$P_J(\sin\varphi)$ (ルジャンドル陪関数の
定義式中で$m=0$とし, 正規化係数($\sqrt{\quad}$の部分)を無くしたもの)
の$J$個の零点(を小さい方から順に並べた
もの)であり, $w_j$は各分点に対応するガウシアンウェイトと呼ばれる重みで,
\begin{equation}
w_j\equiv\frac{2(1-\mu_j^2)}{\{JP_{J-1}(\mu_j)\}^2}
\end{equation}
で与えられる. ここに, $\mu_j\equiv\sin\varphi_j$である.
ある条件のもとでは, この積分公式は完全な近似, すなわちもとの積分と同じ
値を与える.

本ライブラリは, 
スペクトルデータ($s^m_n$) 
$\to$ 格子点上のグリッドデータ($g(\lambda_i,\varphi_j)$) 
の逆変換を行うルーチン群,
等間隔格子点上のグリッドデータ($g(\lambda_i,\varphi_j)$) 
$\to$ スペクトルデータ($s^m_n$) 
の正変換を行うルーチン群,
そして, その他の補助ルーチン群よりなっている.

ここに, 格子点の経度$\lambda_i$は全周を等間隔に$I$-分割した経度で,
$\lambda_i=2\pi i/I\quad (i=0,1,\ldots,I-1)$で(ここでの $i$は虚数
単位ではないので注意),
緯度$\varphi_j$は上述の$J$個のガウス緯度である.
以下のサブルーチンの説明において,
\begin{center}
\begin{tabular}{ll}
\texttt{MM}:& $m$の切断波数$M$\\
\texttt{NN}:& $n$の切断波数$N$\\
\texttt{NM}:& 使いうる$N$の最大値\\
\texttt{MT}:& 三角切断を前提としたプログラムにおける切断波数\\
\texttt{JM}:& ガウス緯度の個数$J$\\
\texttt{IM}:& 東西格子点数$I$\\
\texttt{N}:& 全波数$n$\\
\texttt{M}:& 帯状波数$m$\\
\texttt{J}:& ガウス緯度の番号$j$\\
\texttt{I}:& 東西格子点の番号$i$
\end{tabular}
\end{center}
なる対応関係がある.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{サブルーチンのリスト}

\vspace{1em}
\begin{tabular}{ll}
\texttt{SVINIT} & 初期化\\
\texttt{SVNM2L} & スペクトルデータの格納位置の計算\\
\texttt{SVL2NM} & \texttt{SVNM2L}の逆演算\\
\texttt{SVTS2G} & スペクトルデータからグリッドデータへの変換\\
\texttt{SVTG2S} & グリッドデータからスペクトルデータへの変換\\
\texttt{SVINIC} & \texttt{SVCS2Y}, \texttt{SVCY2S} 等で用いられる配列の初期化\\
\texttt{SVCS2Y/SVCSZY/SVCSWY} & 緯度微分を作用させた逆変換に対応する
スペクトルデータの変換\\
\texttt{SVCY2S/SVCYZS/SVCYWS} & 緯度微分を作用させた正変換に対応する
スペクトルデータの変換\\
\texttt{SVCS2X} & 経度微分に対応するスペクトルデータの変換\\
\texttt{SVINID} & \texttt{SVCLAP} で用いられる配列の初期化\\
\texttt{SVCLAP} & スペクトルデータにラプラシアンを作用, またはその逆演算\\
\texttt{SVCRUP} & 三角切断($N=M$)のスペクトルデータから$N>M$のスペクトルデータ
への詰め替え\\
\texttt{SVCRDN} & \texttt{SVCRUP}の逆の操作
\end{tabular}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{サブルーチンの説明}

\subsection{SVINIT}

\begin{enumerate}

\item 機能
\texttt{SVPACK}の初期化ルーチン.
\texttt{SVPACK}の他のサブルーチンで使われる配列\texttt{IT, T, P, R, JC}
の値を初期化する.

\item 定義

\item 呼び出し方法 
    
\texttt{SVINIT(MM,NM,IM,JM,IT,T,P,R,JC,WP)}
  
\item パラメーターの説明 
    
\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & 入力. $m$の切断波数$M$\\
\texttt{NM} & \texttt{(I)} & 入力. $n$の切断波数$N$の使いうる最大値\\
\texttt{IM} & \texttt{(I)} & 入力. 東西格子点数\\
\texttt{JM} & \texttt{(I)} & 入力. 南北格子点数\\
\texttt{IT}  & \texttt{(I(IM/2))}    & 出力. SVPACKの他のルーチンで用いられる配列\\
\texttt{T}   & \texttt{(D(IM))} & 出力. SVPACKの他のルーチンで用いられる配
列\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & 出力. SVPACKの他のルーチンで用いられる配列\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & 出力. SVPACKの他のルーチンで用いられる配列\\
\texttt{JC}  & \texttt{(I(MM*(2*NM-MM-1)/8+MM))}    & 出力. SVPACKの他のルーチンで用いられる配列\\
\texttt{WP}  & \texttt{(D(JM/2*MM))} 
      & 作業領域.
\end{tabular}

\item 備考

(a) \texttt{MM} は1以上の整数であること.
     また, 通常の三角切断の場合で, かつ緯度方向の微分演算(SVCS2Y等を参照)
     を行う場合, 三角切断の切断波数を \texttt{MT} とすると,
     \texttt{MM=MT},  \texttt{NM=MT+1} と定めれば良い.
     
\texttt{JM}は 2以上の偶数であること. さらに, ISPACKのインストール時に
SSE=avx または SSE=fma とした場合は 8の倍数にしておくと高速になる.
また, SSE=avx512 とした場合は 16の倍数にしておくと高速になる.

\texttt{IM} は \texttt{IM > 2*MM}を満し, かつ 2の羃乗でなければ
ならない.

\texttt{NM} は \texttt{NM} $\ge$ \texttt{MM}を満していなければならない.

(b) \texttt{SVPACK}を使用している間, 配列\texttt{IT, T, P, R, JC}
の内容を変更してはならない.

(c) \texttt{P(JM/2,5+2*MM)}と宣言されている場合, 
   \texttt{P(J,1)}:  $\sin(\varphi_{J/2+j})$,
   \texttt{P(J,2)}:  $\frac12 w_{J/2+j}$, 
   \texttt{P(J,3)}:  $\cos(\varphi_{J/2+j})$,
   \texttt{P(J,4)}:  $1/\cos(\varphi_{J/2+j})$,
が格納される.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVNM2L}

\begin{enumerate}

\item 機能 

全波数と帯状波数からスペクトルデータの格納位置を計算する.

\item 定義

SVPACKにおいて, スペクトルデータ($s^m_n$)
は概要に述べた制限をもとに, 独立な$(2N+1-M)M+N+1$個
の成分をその長さの配列に格納して扱う.

格納順は, 切断波数に依存してしまうが, 計算に便利なように
以下のような順序になっている.
\begin{eqnarray*}
&& s^0_0, s^0_1, \ldots, s^0_N,\\
&& \mbox{Re}(s^1_1), \mbox{Im}(s^1_1),
\mbox{Re}(s^1_2), \mbox{Im}(s^1_2), \ldots, 
\mbox{Re}(s^1_N), \mbox{Im}(s^1_N),\\
&&\ldots,\\
&& \mbox{Re}(s^M_M), \mbox{Im}(s^M_M), \ldots,
\ldots \mbox{Re}(s^M_N), \mbox{Im}(s^M_N)\\
\end{eqnarray*}
このサブルーチンは, $s^m_n$の全波数$n$, および帯状波数$m$か
ら$s^m_n$の配列中の格納位置を求めるものである.

\item 呼び出し方法 
    
\texttt{SVNM2L(NN,N,M,L)}
  
\item パラメーターの説明 

\begin{tabular}{lll}
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数$N$\\
\texttt{N} & \texttt{(I)} & 入力. 全波数\\
\texttt{M} & \texttt{(I)} & 入力. 帯状波数(備考参照)\\
\texttt{L} & \texttt{(I)} & 出力. スペクトルデータの格納位置
\end{tabular}

\item 備考

\texttt{M} $>$ 0 なら $m=$ \texttt{M}, $n=$ \texttt{N}として$\mbox{Re}(s^m_n)$の格納
位置を, \texttt{M} $<$ 0 なら $m=$ \texttt{-M}, $n=$ \texttt{N}として
$\mbox{Im}(s^m_n)$の格納位置を返す.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVL2NM}

\begin{enumerate}

\item 機能 
\texttt{SVNM2L}の逆演算, すなわち, スペクトルデータの格納位置から全波数と
帯状波数を求める.

\item 定義

\texttt{SVNM2L}の項を参照

\item 呼び出し方法 
    
\texttt{SVL2NM(NN,L,N,M)}
  
\item パラメーターの説明 

\begin{tabular}{lll}
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数$N$\\
\texttt{L} & \texttt{(I)} & 入力. スペクトルデータの格納位置\\
\texttt{N} & \texttt{(I)} & 出力. 全波数\\
\texttt{M} & \texttt{(I)} & 出力. 帯状波数
\end{tabular}

\item 備考

 \texttt{M} の正負についての意味づけは\texttt{SVNM2L}と同じである.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVTS2G}

\begin{enumerate}

\item 機能 

スペクトルデータからグリッドデータへの変換を行う.

\item 定義

スペクトル逆変換(概要を参照)によりスペクトルデータ($s^m_n$)
から格子点上のグリッドデータ($g(\lambda_i,\varphi_j)$)を求める.

\item 呼び出し方法 

\texttt{SVTS2G(MM,NM,NN,IM,JM,S,G,IT,T,P,R,JC,WS,W,IPOW)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MM} & \texttt{(I)} & 入力. $m$の切断波数\\
\texttt{NM} & \texttt{(I)} & 入力. $n$の切断波数の最大値\\
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数
(\texttt{MM}$\le$\texttt{NN}$\le$\texttt{NM}であること)\\
\texttt{IM} & \texttt{(I)} & 入力. 東西格子点数\\
\texttt{JM} & \texttt{(I)} & 入力. 南北格子点数\\
\texttt{S} & \texttt{(D((2*NN+1-MM)*MM+NN+1))} & 入力. $s^m_n$が格納されている配列\\
\texttt{G} & \texttt{(D(JM*IM))} & 出力. $g(\lambda_i,\varphi_j)$が格納される配列\\
\texttt{IT}  & \texttt{(I(IM/2))}    & 入力. SVINITで与えられた配列\\
\texttt{T}   & \texttt{(D(IM))} & 入力. SVINITで与えられた配列\\
\texttt{P}  & \texttt{(D(JM/2*(2*MM+5)))} 
      & 入力. SVINITで与えられた配列\\
\texttt{R}  & \multicolumn{2}{l}{\texttt{(D(((MM+1)*(2*NM-MM-1)+1)/4*3+(2*NM-MM)*(MM+1)/2))}}\\
 &     & 入力. SVINITで与えられた配列\\
\texttt{JC}  & \texttt{(I(MM*(2*NM-MM-1)/8+MM))}  & 入力. SVINITで与えられた配列\\
\texttt{WS} & \texttt{(D((NN+1)*2*(MM+1))} & 作業領域\\
\texttt{W} & \texttt{(D(JM*IM*2))} & 作業領域\\
\texttt{IPOW} & \texttt{(I)} & 入力. 逆変換と同時に作用させる
                      $1/\cos\varphi$の次数. \\
& & 0から2までの整数.
\end{tabular}

\item 備考

(a) \texttt{G(0:IM-1,JM)}と宣言されている場合, \texttt{G(I,J)}には
    $g(\lambda_i,\varphi_j)$が格納される.

(b) \texttt{G, W}の先頭アドレスは, 
ISPACKのインストール時に SSE=avx または SSE=fma とし, かつ
JMを8の倍数とした場合は, 32バイト境界に
合っていなければならない.
また, SSE=avx512 とし, かつ
JMを 16の倍数とした場合は, 64バイト境界に
合っていなければならない.

(c) \texttt{IPOW}$=l$とすると, $g(\lambda_i,\varphi_j)$の
    かわりに $(\cos\varphi_j)^{-l}g(\lambda_i,\varphi_j)$ が出力
    される.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVTG2S}

\begin{enumerate}

\item 機能 

グリッドデータからスペクトルデータへの変換を行う.

\item 定義

スペクトル正変換(概要を参照)により格子点上の
グリッドデータ($g(\lambda_i,\varphi_j)$)
からスペクトルデータ($s^m_n$)を求める.

\item 呼び出し方法 

\texttt{SVTG2S(MM,NM,NN,IM,JM,S,G,IT,T,P,R,JC,WS,W,IPOW)}

  
\item パラメーターの説明(殆んど SVTS2G の項と同じであるので,
異なる部分のみについて記述する).

\begin{tabular}{lll}
\texttt{S} & \texttt{(D((2*NN+1-MM)*MM+NN+1))} & 出力 $s^m_n$が格納される配列\\
\texttt{G} & \texttt{(D(JM*IM))} & 入力. $g(\lambda_i,\varphi_j)$が
 格納されている配列\\
\texttt{IPOW} & \texttt{(I)} & 入力. 正変換と同時に作用させる
                      $1/\cos\varphi$の次数. \\
& & 0から2までの整数.
\end{tabular}

\item 備考

(a) \texttt{G(0:IM-1,JM)}と宣言されている場合, \texttt{G(I,J)}には
    $g(\lambda_i,\varphi_j)$を格納すること.

(b) \texttt{IPOW}$=l$とすると, $g(\lambda_i,\varphi_j)$の
    かわりに $(\cos\varphi_j)^{-l}g(\lambda_i,\varphi_j)$ が入力
    になる. 
   
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVINIC}

\begin{enumerate}

\item 機能 

SVCS2Y, SVCY2S 等で用いられる配列の\texttt{C}の初期化.

\item 定義

SVCS2Y, SVCY2Sの項を参照.

\item 呼び出し方法 

\texttt{SVINIC(MT,C)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{C} & \texttt{(D((MT+1)*(MT+1)))} & 出力. SVCS2Y, SVCY2S 等で用いられる配列
\end{tabular}

\item 備考

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCS2Y/SVCSZY/SVCSWY}

\begin{enumerate}

\item 機能 

緯度微分を作用させた逆変換に対応するスペクトルデータの変換を行う.

\item 定義

スペクトル逆変換(ただし, ここでは $N=M$の三角切断とする)が
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^M_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
と定義されているとき, $g$に$\cos\varphi\frac{\partial}{\partial\varphi}$
を作用させた結果は
\begin{equation}
\cos\varphi\frac{\partial}{\partial\varphi}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^{M+1}_{n=|m|}
(s_y)^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
のように$n$方向の切断波数$N=M+1$のスペクトル逆変換で表せる. この
サブルーチンは, $s^m_n$から$(s_y)^m_n$を求めるものである.

\item 呼び出し方法 

\texttt{SVCS2Y(MT,S,SY,C)}\\
\texttt{SVCSZY(MT,S,SY,C)}\\
\texttt{SVCSWY(MT,M,S,SY,C)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{M} & \texttt{(I)} & 入力. 帯状波数\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & 入力. $s^m_n$が格納されている配列\\
\texttt{SY} & \texttt{(D((MT+4)*MT+2))} & 出力. $(s_y)^m_n$が格納される配列\\
\texttt{C} & \texttt{(D((MT+1)*(MT+1)))} & 入力. SVINICで与えられる配列
\end{tabular}

\item 備考

(a) \texttt{SVCS2Y(MT,S,SY,C)} と 
    \texttt{SVTS2G(MT,NM,MT+1,IM,JM,SY,G,IT,T,P,Q,R,WS,WG,W,1)}
    とを連続して CALL すれば, \texttt{G}に緯度微分が返されることになる.

(b) \texttt{SVCSZY}は帯状成分$m=0$についての処理を行う.
    この場合,
    \texttt{S(0:MT)}, \texttt{SY(0:MT+1)}と宣言してあれば, \texttt{S(N)}, \texttt{SY(N)}
    にはそれぞれ$s^0_n$, $(s_y)^0_n$が対応する.

(c) \texttt{SVCSWY}はある波成分$m>0$についての処理を行う.
    この場合,
    \texttt{S(2,M:MT)}, \texttt{SY(2,M:MT+1)}と宣言してあれば, 
    \texttt{S(1,N)}, \texttt{S(2,N)}, \texttt{SY(1,N)}, \texttt{SY(2,N)}にはそれぞれ
    $\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$, $\mbox{Re}((s_y)^m_n)$, $\mbox{Im}((s_y)^m_n)$, 
    が対応する.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCY2S/SVCYZS/SVCYWS}

\begin{enumerate}

\item 機能 

緯度微分を作用させた正変換に対応するスペクトルデータの変換を行う.

\item 定義

以下のような変形されたスペクトル正変換
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)
\left(-\cos\varphi\frac{\partial}{\partial\varphi}P^m_n(\sin\varphi)\right)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\quad 
\end{equation}
(ただし, $m$, $n$ は切断波数 $M$ の三角切断の範囲とする)
を計算したい場合(これは, ベクトル場の発散の計算などで現れる), 
$g$の通常のスペクトル正変換
\begin{equation}
(s_y)^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
g(\lambda,\varphi)
P^m_n(\sin\varphi)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\quad (ただし, n の切断波数 N=M+1)
\end{equation}
の結果から$s^m_n$を求めることができる. 
このサブルーチンは, $(s_y)^m_n$から$s^m_n$を求めるものである.

\item 呼び出し方法 

\texttt{SVCY2S(MT,SY,S,C)}\\
\texttt{SVCYZS(MT,SY,S,C)}\\
\texttt{SVCYWS(MT,M,SY,S,C)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{M} & \texttt{(I)} & 入力. 帯状波数\\
\texttt{SY} & \texttt{(D((MT+4)*MT+2))} & 入力. $(s_y)^m_n$が格納されている配列\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & 出力. $s^m_n$が格納される配列\\
\texttt{C} & \texttt{(D((MT+1)*(MT+1)))} & 入力. SVINICで与えられる配列
\end{tabular}

\item 備考

(a) \texttt{SVTG2S(MT,NM,MT+1,IM,JM,SY,G,IT,T,P,Q,R,WS,WG,W,1)} と
    \texttt{SVCY2S(MT,SY,S,C)} とを連続して CALL すれば,
    ベクトル場の緯度方向成分の発散の正変換に相当する計算
\begin{equation}
s^m_n=\frac1{4\pi}\int^{2\pi}_0\int^{\pi/2}_{-\pi/2}
\frac{\partial}{\cos\varphi\partial\varphi}
\left(\cos\varphi g(\lambda,\varphi)\right)
P^m_n(\sin\varphi)
e^{-im\lambda}\cos\varphi d\varphi
d\lambda .
\end{equation}
が行われ, \texttt{S}に$s^m_n$が返されることになる
(部分積分していることと, \texttt{IPOW=1} としていることに注意).

(b) \texttt{SVCYZS}は帯状成分$m=0$についての処理を行う.
    この場合,
    \texttt{S(0:MT)}, \texttt{SY(0:MT+1)}と宣言してあれば, \texttt{S(N)}, \texttt{SY(N)}
    にはそれぞれ$s^0_n$, $(s_y)^0_n$が対応する.

(c) \texttt{SVCYWS}はある波成分$m>0$についての処理を行う.
    この場合,
    \texttt{S(2,M:MT)}, \texttt{SY(2,M:MT+1)}と宣言してあれば, 
    \texttt{S(1,N)}, \texttt{S(2,N)}, \texttt{SY(1,N)}, \texttt{SY(2,N)}にはそれぞれ
    $\mbox{Re}(s^m_n)$, $\mbox{Im}(s^m_n)$, $\mbox{Re}((s_y)^m_n)$, $\mbox{Im}((s_y)^m_n)$, 
    が対応する.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCS2X}

\begin{enumerate}

\item 機能 

経度微分に対応するスペクトルデータの変換を行う.

\item 定義

スペクトル逆変換(ただし, ここでは $N=M$の三角切断とする)が
\begin{equation}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^M_{n=|m|}
s^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
と定義されているとき, $g$に$\frac{\partial}{\partial\lambda}$
を作用させた結果は
\begin{equation}
\frac{\partial}{\partial\lambda}
g(\lambda,\varphi)=\sum^M_{m=-M}\sum^{M}_{n=|m|}
(s_x)^m_nP^m_n(\sin\varphi)e^{im\lambda}
\end{equation}
のように表せる. ここに, $(s_x)^m_n=ims^m_n$ である.
このサブルーチンは, $s^m_n$から$(s_x)^m_n=ims^m_n$を求めるものである.

\item 呼び出し方法 

\texttt{SVCS2X(MT,S,SX)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & 入力. $s^m_n$が格納されている配列\\
\texttt{SX} & \texttt{(D((MT+1)*(MT+1)))} & 出力. $(s_x)^m_n$が格納される配列
\end{tabular}

\item 備考

(a) スペクトルデータ $(s_x)^m_n$ の並び順は $s^m_n$ と同じである.
    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVINID}

\begin{enumerate}

\item 機能 

SVCLAP で用いられる配列の\texttt{D}の初期化.

\item 定義

SVCLAP の項を参照.

\item 呼び出し方法 

\texttt{SVINID(MT,D)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{D} & \texttt{(D((MT+1)*(MT+1)*2))} & 出力. SVCLAP で用いられる配列
\end{tabular}

\item 備考

    
\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCLAP}

\begin{enumerate}

\item 機能 
スペクトルデータにラプラシアンを作用させる, またはその逆演算を行う.

\item 定義

球面調和関数展開(ただし, ここでは $N=M$の三角切断とする)
\begin{equation}
g(\lambda,\varphi)=\sum^M_{n=0}\sum^n_{m=-n}
s^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
に対して, 水平Laplacian
\begin{equation}
\nabla^2\equiv
\frac{\partial^2}{\cos^2\varphi\partial\lambda^2}
+\frac{\partial}{\cos\varphi\partial\varphi}\left(\cos\varphi\frac{\partial}{\partial\varphi}\right)
\end{equation}
を作用させると, 球面調和関数の性質から, 
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}-n(n+1)a^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
となる. そこで,
\begin{equation}
(s_l)^m_n\equiv -n(n+1)s^m_n
\end{equation}
を導入すると, 
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}(s_l)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
と表せる. 
また, 逆に
\begin{equation}
\nabla^2 g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}s^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
であるとき, 
\begin{equation}
(s_l)^m_n\equiv -\frac1{n(n+1)}s^m_n
\end{equation}
を導入すると, 
\begin{equation}
g(\lambda,\varphi)
=\sum^M_{n=0}\sum^n_{m=-n}(s_l)^m_nP^m_n(\sin\varphi)e^{im\lambda}.
\end{equation}
と表せる. 

本サブルーチンは,
$s^m_n$から$(s_l)^m_n = -n(n+1)s^m_n$の計算, 
またはその逆演算: $s^m_n$から$(s_l)^m_n = -s^m_n/(n(n+1))$, を
行うものである. 

\item 呼び出し方法 
    
\texttt{SVCLAP(MT,S,SL,D,IFLAG)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & 入力. $s^m_n$が格納されている配列\\
\texttt{SL} & \texttt{(D((MT+1)*(MT+1)))} & 出力. $(s_l)^m_n$が格納される配列\\
\texttt{D} & \texttt{(D((MT+1)*(MT+1)*2))} & 入力.
SVINIDで初期化された配列\\
\texttt{IFLAG} & \texttt{(I)} & 入力. \texttt{IFLAG=1}のときラプラシアンの演算を,
\texttt{IFLAG=2}のとき逆演算を行う.
\end{tabular}

\item 備考

(a) \texttt{IFLAG=2}の場合, $n=0$となる $(s_l)^0_0$ については
$s^0_0$がそのまま代入される.

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCRUP}

\begin{enumerate}

\item 機能

三角切断($N=M$)のスペクトルデータを
$N>M$のスペクトルデータに詰め替える.

\item 定義

\texttt{SVCS2Y}の出力結果と\texttt{SVCS2X}の出力結果を合成したいことは
しばしば生じるが, 両者は$n$方向の切断波数$N$が異なるスペクトルデータ
であるため, そのまま足すことができない . 本サブルーチンは, そのような
用途に応えるために,
三角切断($N=M$)のスペクトルデータ($s^m_n$)から,
$N>M$のスペクトルデータ($(s_r)^m_n$)への詰め替えを行うものである.
なお, $(s_r)^m_n$の$n>M$の部分には$0$が代入される.

\item 呼び出し方法 
    
\texttt{SVCRUP(MT,NN,S,SR)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & 入力. $s^m_n$が格納されている配列\\
\texttt{SR} & \texttt{(D((2*NN+1-MT)*MT+NN+1))} & 出力. $(s_r)^m_n$が格納される配列
\end{tabular}

\item 備考

\end{enumerate}

%---------------------------------------------------------------------

\subsection{SVCRDN}

\begin{enumerate}

\item 機能

\texttt{SVCRUP}の逆の操作, すなわち, $N>M$のスペクトルデータから
三角切断($N=M$)のスペクトルデータへの詰め替えを行う.


\item 定義

本サブルーチンは, 
$N>M$のスペクトルデータ($(s_r)^m_n$)から
三角切断($N=M$)のスペクトルデータ($s^m_n$)
への詰め替えを行うものである.
なお, $(s_r)^m_n$の$n>M$の部分の情報は捨てられる.

\item 呼び出し方法 
    
\texttt{SVCRDN(MT,NN,SR,S)}
  
\item パラメーターの説明

\begin{tabular}{lll}
\texttt{MT} & \texttt{(I)} & 入力. 三角切断の切断波数\\
\texttt{NN} & \texttt{(I)} & 入力. $n$の切断波数\\
\texttt{SR} & \texttt{(D((2*NN+1-MT)*MT+NN+1))} & 入力 $(s_r)^m_n$が格納されている配列
\\
\texttt{S} & \texttt{(D((MT+1)*(MT+1)))} & 出力. $s^m_n$が格納される配列
\end{tabular}

\item 備考

\end{enumerate}


\end{document}
